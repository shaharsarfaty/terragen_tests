#include <iostream>
#define RL_DEBUG_COUNT_POSITIVE_ENTRIES
#define RL_DEBUG_VALUE_UPDATE

#include "Tests.h"
#include "DeepTests.h"
#include "DeepGraphicGridTester.h"
using namespace std;
int main() {
//	 configuring hyper-parameters
//	Tests::config cfg;
//	cfg.learning_rate = 0.2;
//	cfg.discount_factor = 0.9;
//	cfg.initial_epsilon = 0.2;
//	cfg.reward_multiplier = 10.0;
//	cfg.negative_incentive_reward = -0.2;
//	cfg.number_of_steps = 5;
//	cfg.final_part_of_epsilon = 1;
//	cfg.number_of_reloadings = 10000;
//	cfg.max_number_of_steps_before_break = 1000;
//
//	// configuring debug files
//	Tests::DebugFiles debug_files;
//	debug_files.root_path = new string(".");
//
//	// creating model
////	TagManagerModel tag_manager_model;
//	SimpleDesignModel simple_design_model;
//
//	// testing
//	int number_of_different_tests = 1;
//	for (int i = 0; i < number_of_different_tests; i++) {
//		Tests::test_templte(cfg, debug_files, simple_design_model);
//	}
	cout << "Start" << endl;
//	DeepTests::firstTestWithExternalSimpleModel();
//	DeepTests::graphicGridModelTest();
//	DeepTests::experienceSerializationTest();
//	DeepTests::graphicGridLearningTest();
//	DeepTests::graphicBatchGridLearningTest();
	DeepGraphicGridTester* tester = new DeepGraphicGridTester();
	tester->test_1();
	cout << "End" << endl;

	return 0;
}

