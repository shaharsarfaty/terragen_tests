#include "DeepTests.h"

using namespace std;
using namespace mxnet::cpp;

void DeepTests::firstTestWithExternalSimpleModel() {
	EveryStepRewardLadderModel model(40);
	model.init();
	HyperParameters hyper_parameters;
	hyper_parameters.batch_size = 15;
	hyper_parameters.discount_factor = 0;
	hyper_parameters.experience_window_size = 200;
	hyper_parameters.weights_initialization_scale = 1;
	hyper_parameters.lr_schedulare.setValue(0.0005);
	hyper_parameters.lr_schedulare.setDecayRate(0.5);
	hyper_parameters.lr_schedulare.setNumberOfStepsBetweenUpdates(100000);
	hyper_parameters.lr_schedulare.setMinimalValue(1e-6);
	hyper_parameters.epsilon_schedulare.setValue(0.9);
	hyper_parameters.epsilon_schedulare.setDecayRate(0.8);
	hyper_parameters.epsilon_schedulare.setNumberOfStepsBetweenUpdates(1000);
	hyper_parameters.epsilon_schedulare.setMinimalValue(0.1);
	hyper_parameters.weight_decay_factor = 1e-4;
	hyper_parameters.hot_ground_incentive_reward = -0.2;
	hyper_parameters.number_of_training_iterations_for_each_observation = 6;
	hyper_parameters.gradient_rescaler = 1.0 / hyper_parameters.batch_size;
	hyper_parameters.gradient_clipping_range = 1.0;
	hyper_parameters.hidden_layers_sizes = { 70, 50 };
	hyper_parameters.number_of_backwords_passes_before_updating_target_network = 5;
	DomainParameters domain_parameters;
	domain_parameters.number_of_registers = 2;
	domain_parameters.number_of_coverage_metrics = 1;
	domain_parameters.total_number_of_state_bits = model.getTotalNumberOfBits();
	domain_parameters.number_of_actions = 5;
	FilesNames files_names;
	files_names.domain_parameters_file_name = "domain_parameters_dump";
	files_names.experience_file_name = "experience_dump";
	files_names.hyper_parameters_file_name = "hyper_parameters_dump";
	files_names.network_inference_parameters_file_name = "network_inference_parameters_dump";
	files_names.network_training_parameters_file_name = "network_training_parameters_dump";
	files_names.running_parameters_file_name = "running_parameters_dump";
	DebugFiles debug_files;
	debug_files.root_path = new string(".");
	createDebugFilesHierarchy(hyper_parameters, debug_files);

	Observation initial_observation((int) domain_parameters.number_of_registers, model.getRegistersInNewCStringArray(),
			(int) domain_parameters.number_of_coverage_metrics, model.getCoverageInNewCStringArray());
	DeepReinforcementLearner *learner = new DeepReinforcementLearner(domain_parameters, hyper_parameters,
			initial_observation, files_names);
	string dumping_file_name = "dumping_file_name";
	vector<State> all_states;
	getSpecialStatesListForAccuracyCheck64levels(all_states, model, domain_parameters);
	int next_action;

	size_t loop_count = 0, iterations = 0, total_iterations = 100000, episode = 0;
	do {
		if (iterations > 100 && iterations % 100 == 0) {
			cout << "\niterations:\t" << iterations << endl;
		}
		next_action = learner->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
				model.getCoverageInNewCStringArray(), 0);
		cout << "(" << model.getLevel() << "," << next_action << "),";
		if (next_action == -1) {
			cout << endl;
			printCurrentProgress(iterations, loop_count, total_iterations);
			model.init();
			learner->saveToFiles(dumping_file_name);
			delete learner;
			learner = new DeepReinforcementLearner(dumping_file_name, initial_observation);
			++loop_count;
			iterations = 0;
			everyEpisodeDebug(learner, episode, loop_count, all_states, debug_files);
			++episode;
			continue;
		}
		model.makeStep(next_action);
		++loop_count;
		++iterations;
	} while (loop_count < total_iterations);

	delete learner;
}

void DeepTests::getStatesListForAccuracyCheck(std::vector<State> &states, DesignModel &model,
		DomainParameters domain_parameters) {
	model.init();
	const char **reg;
	const char **cov;
	for (int i = 0; i < model.getNumberOfLevels(); ++i) {
		reg = model.getRegistersInNewCStringArray();
		cov = model.getCoverageInNewCStringArray();
		states.push_back(
				State(domain_parameters.number_of_registers, reg, domain_parameters.number_of_coverage_metrics, cov));
		for (size_t j = 0; j < domain_parameters.number_of_registers; ++j) {
			delete[] reg[j];
		}
		delete[] reg;
		for (size_t j = 0; j < domain_parameters.number_of_coverage_metrics; ++j) {
			delete[] cov[j];
		}
		delete[] cov;
		model.makeStep(1);
	}
	model.init();
}

void DeepTests::getSpecialStatesListForAccuracyCheck64levels(std::vector<State> &states, EveryStepRewardLadderModel &model,
		DomainParameters domain_parameters) {

	vector<size_t> list_of_coverages_to_form_states = { 0, 10, 60 };
	for (size_t coverage : list_of_coverages_to_form_states) {
		createThreeStatesForGivenCoverageAndPush(states, model, domain_parameters, coverage);
	}
	model.init();
}

void DeepTests::createThreeStatesForGivenCoverageAndPush(std::vector<State> &states, EveryStepRewardLadderModel &model,
		DomainParameters domain_parameters, size_t coverage_up_to_level) {
	const char **reg;
	const char **cov;
	model.init();
	int actual_up;

	/* On the edge of coverage rise */
	for (size_t i = 0; i < coverage_up_to_level; ++i) {
		if (model.getFlippingLevel() != -1 && model.getLevel() >= model.getFlippingLevel()) {
			actual_up = 0;
		} else {
			actual_up = 1;
		}
		model.makeStep(actual_up);
	}
	reg = model.getRegistersInNewCStringArray();
	cov = model.getCoverageInNewCStringArray();
	states.push_back(State(domain_parameters.number_of_registers, reg, domain_parameters.number_of_coverage_metrics, cov));
	deleteRegsAndCovs(reg, cov, domain_parameters);

	/* Level zero */
	if (coverage_up_to_level == 0) {
		return;
	}

	for (size_t i = 0; i < coverage_up_to_level; ++i) {
		model.makeStep(2);
	}
	reg = model.getRegistersInNewCStringArray();
	cov = model.getCoverageInNewCStringArray();
	states.push_back(State(domain_parameters.number_of_registers, reg, domain_parameters.number_of_coverage_metrics, cov));
	deleteRegsAndCovs(reg, cov, domain_parameters);

	/* Mid-way level, regarding to coverage */
	if (coverage_up_to_level / 2 == 0) {
		return;
	}
	for (size_t i = 0; i < coverage_up_to_level / 2; ++i) {
		if (model.getFlippingLevel() != -1 && model.getLevel() >= model.getFlippingLevel()) {
			actual_up = 0;
		} else {
			actual_up = 1;
		}
		model.makeStep(actual_up);
	}
	reg = model.getRegistersInNewCStringArray();
	cov = model.getCoverageInNewCStringArray();
	states.push_back(State(domain_parameters.number_of_registers, reg, domain_parameters.number_of_coverage_metrics, cov));
	deleteRegsAndCovs(reg, cov, domain_parameters);
}
void DeepTests::getAllStatesListForAccuracyCheck(std::vector<State> &states, DesignModel &model,
		DomainParameters domain_parameters) {
	model.init();
	const char **reg;
	const char **cov;

	for (int i = 0; i < model.getNumberOfLevels(); ++i) {
		for (int j = i; j >= 0; j--) {
			reg = model.getRegistersInNewCStringArray();
			cov = model.getCoverageInNewCStringArray();
			states.push_back(
					State(domain_parameters.number_of_registers, reg, domain_parameters.number_of_coverage_metrics, cov));
			for (size_t j = 0; j < domain_parameters.number_of_registers; ++j) {
				delete[] reg[j];
			}
			delete[] reg;
			for (size_t j = 0; j < domain_parameters.number_of_coverage_metrics; ++j) {
				delete[] cov[j];
			}
			delete[] cov;
			model.makeStep(2);
		}
		for (int j = i; j >= 0; j--) {
			model.makeStep(1);
		}
	}
	model.init();
}

void DeepTests::setHumanCraftedActionIfNeeded(int &next_action, size_t episode,
		size_t number_of_first_episodes_to_be_crafted, DeepReinforcementLearner *learner) {
	float rand_0_to_1;
	if (next_action != -1 && episode < number_of_first_episodes_to_be_crafted) {
		rand_0_to_1 = learner->getRandBetweenZeroAndOneFromLearnerInstance();
		if (rand_0_to_1 < 0.1) {
			next_action = EveryStepRewardLadderModel::NOP;
		} else if (rand_0_to_1 < 0.8) {
			next_action = EveryStepRewardLadderModel::GO_UP;
		} else {
			next_action = EveryStepRewardLadderModel::GO_DOWN;
		}
		learner->getCurrentObservation().previous_action = next_action;
	}
}
