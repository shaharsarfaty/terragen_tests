#include "TagManagerModel.h"
using namespace std;
/* Magic line for visual studio - ignores pre-processor's errors of 'cpystr not safe' */
#ifdef _WIN32
//#pragma warning(disable:4996)
#endif

TagManagerModel::TagManagerModel() : DesignModel(0, 1, 6) {
	for (int i = 0; i < number_of_coverage_metrics; i++) {
			coverage_metric_names[i] = new char[16];
			string name(to_string(i));
			name.append(" tags occupied");
//			strcpy_s(coverage_metric_names[i], 16, name.c_str());
			strcpy(coverage_metric_names[i], name.c_str());
		}

		for (int i = 0; i < number_of_actions; i++) {
			action_names[i] = new char[9];
			string name("action_");
			name.append(to_string(i));
//			strcpy_s(action_names[i], 9, name.c_str());
			strcpy(action_names[i], name.c_str());
		}
}

TagManagerModel::~TagManagerModel() {
	for (int i = 0; i < number_of_coverage_metrics; i++) {
		delete[] coverage_metric_names[i];
	}
	for (int i = 0; i < number_of_actions; i++) {
		delete[] action_names[i];
	}
}

void TagManagerModel::init() {
	tags = "0000";
	for (int number_of_occupied_tags = 0; number_of_occupied_tags < 5; number_of_occupied_tags++) {
		for (int request = 0; request < 2; request++) {
			for (int ret_tag = 0; ret_tag < 2; ret_tag++) {
				for (int free_res = 0; free_res < 2; free_res++) {
					coverage[number_of_occupied_tags][request][ret_tag][free_res] = '0';
				}
			}
			/* illegal bins - initialized to 1 in our implementation */
			coverage[number_of_occupied_tags][request][RETURN_OFF][FREE_REJECTED] = '1';
		}
	}
	/* illegal bins - initialized to 1 in our implementation */
	for (int request = 0; request < 2; request++) {
		coverage[0][request][RETURN_ON][FREE_ACCEPTED] = '1';
		coverage[number_of_tags][request][RETURN_ON][FREE_REJECTED] = '1';
	}
}

void TagManagerModel::makeStep(DesignModel::action_type action) {
	switch (action) {
	case NOP:
		nop();
		break;
	case GRAB:
		grab();
		break;
	case FREE_GRABBED:
		freeGrabbed();
		break;
	case FREE_NON_GRABBED:
		freeNonGrabbed();
		break;
	case GRAB_AND_FREE_GRABBED:
		grabAndFreeGrabbed();
		break;
	case GRAB_AND_FREE_NON_GRABBED:
		grabAndFreeNonGrabbed();
		break;
	default:
		break;
	}
}

void TagManagerModel::nop() {
	coverage[getNumberOfOccupiedTags()][REQ_OFF][RETURN_OFF][FREE_ACCEPTED] = '1';
}

void TagManagerModel::grab() {
	unsigned int number_of_occupied_tags = getNumberOfOccupiedTags();
	for (int free_res = 0; free_res < 2; free_res++) {
		coverage[number_of_occupied_tags][REQ_ON][RETURN_OFF][free_res] = '1';
	}
	if (number_of_occupied_tags == tags.size()) return;
	grabFreeTagIfExisted();
}

void TagManagerModel::freeGrabbed() {
	int number_of_occupied_tags = getNumberOfOccupiedTags();
	if (number_of_occupied_tags != 0) {
		coverage[number_of_occupied_tags][REQ_OFF][RETURN_ON][FREE_ACCEPTED] = '1';
	}
	else {
		coverage[number_of_occupied_tags][REQ_OFF][RETURN_ON][FREE_REJECTED] = '1';
	}
	size_t last_take_tag_index = tags.find_last_of('1');
	if (last_take_tag_index == string::npos) return;
	tags.at(last_take_tag_index) = '0';
}

void TagManagerModel::freeNonGrabbed() {
	int number_of_occupied_tags = getNumberOfOccupiedTags();
	if (number_of_occupied_tags != number_of_tags) {
		coverage[number_of_occupied_tags][REQ_OFF][RETURN_ON][FREE_REJECTED] = '1';
	}
	else {
		coverage[number_of_occupied_tags][REQ_OFF][RETURN_ON][FREE_ACCEPTED] = '1';
	}
}

void TagManagerModel::grabAndFreeGrabbed() {
	unsigned int number_of_occupied_tags = getNumberOfOccupiedTags();
	if (number_of_occupied_tags != 0) {
		coverage[number_of_occupied_tags][REQ_ON][RETURN_ON][FREE_ACCEPTED] = '1';
	}
	else {
		coverage[number_of_occupied_tags][REQ_ON][RETURN_ON][FREE_REJECTED] = '1';
	}
	if (number_of_occupied_tags != tags.size()) {
		grabFreeTagIfExisted();
	}

	size_t last_take_tag_index = tags.find_last_of('1');
	if (last_take_tag_index != string::npos) {
		tags.at(last_take_tag_index) = '0';
	}
}

void TagManagerModel::grabAndFreeNonGrabbed() {
	unsigned int number_of_occupied_tags = getNumberOfOccupiedTags();
	coverage[number_of_occupied_tags][REQ_ON][RETURN_ON][FREE_REJECTED] = '1';
	
	if (number_of_occupied_tags != tags.size()) {
		grabFreeTagIfExisted();
	}
}

int TagManagerModel::getNumberOfOccupiedTags() {
	int count = 0;
	char bit;
	for (unsigned int i = 0; i < tags.size(); i++) {
		bit = tags.at(i);
		assert(bit == '0' || bit == '1');
		if (bit == '1') count++;
	}
	return count;
}

void TagManagerModel::grabFreeTagIfExisted() {
	size_t first_free_tag = tags.find_first_of('0');
	if (first_free_tag == string::npos) return;
	tags.at(first_free_tag) = '1';
}

const char** TagManagerModel::getRegistersInNewCStringArray() {
	char** registers_content = new char* [1];
	registers_content[0] = new char[tags.size() + 1];
	strcpy(registers_content[0], tags.c_str());
	return (const char**)registers_content;
}

const char** TagManagerModel::getCoverageInNewCStringArray() {
	char** coverage_content = new char* [1];
	string coverage_string = "";
	for (int number_of_occupied_tags = 0; number_of_occupied_tags < 5; number_of_occupied_tags++) {
		for (int request = 0; request < 2; request++) {
			for (int ret_tag = 0; ret_tag < 2; ret_tag++) {
				for (int free_res = 0; free_res < 2; free_res++) {
					coverage_string.append(1, coverage[number_of_occupied_tags][request][ret_tag][free_res]);
				}
			}
		}
	}
#ifdef RL_DEBUG
	//cout << coverage_string << "\n";
#endif
	coverage_content[0] = new char[5*2*2*2 + 1];
	strcpy(coverage_content[0], coverage_string.c_str());
	return (const char**)coverage_content;
}

void TagManagerModel::printModel(std::ostream& output_stream) {
	output_stream << "Tags:\t" << tags << "\t";
	output_stream << "Coverage:\t";

	string coverage_string = "";
	for (int number_of_occupied_tags = 0; number_of_occupied_tags < 5; number_of_occupied_tags++) {
		for (int request = 0; request < 2; request++) {
			for (int ret_tag = 0; ret_tag < 2; ret_tag++) {
				for (int free_res = 0; free_res < 2; free_res++) {
					coverage_string.append(&coverage[number_of_occupied_tags][request][ret_tag][free_res]);
				}
			}
		}
	}
	output_stream << coverage_string << "\n";
}

void TagManagerModel::printMissingCoverageWithNames(ostream& os) {
	string coverage_string(getCoverageInNewCStringArray()[0]);
	os << "Uncovered bins: " << "\n";
	for (unsigned int i = 0; i < coverage_string.size(); i++) {
		if (coverage_string.at(i) == '0') {
			if (i < 10) {
				os << "bin #" << i << ",\t\t";
			}
			else {
				os << "bin #" << i << ",\t";
			}
			os << "TAGS TAKEN:\t" << (i / 8) << ",\t";
			os << "REQUEST:\t" << (i % 8 < 4 ? "0" : "1") << ",\t";
			os << "RETURN:\t" << (i % 4 < 2 ? "0" : "1") << ",\t";
			os << "FREE ACCEPTED:\t" << (i % 2 < 1 ? "0" : "1") << ",\t";
			os << endl;
		}
	}
	os << endl;
}
