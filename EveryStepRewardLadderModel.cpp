/*
 * EveryStepRewardLadderModel.cpp
 *
 *  Created on: 8 Jan 2020
 *      Author: sarfaty
 */

#include "EveryStepRewardLadderModel.h"
using namespace std;

EveryStepRewardLadderModel::EveryStepRewardLadderModel(int c_level_to_flip_nop_and_up) :
		DesignModel(1, 1, 3), coverage(MAX_LEVEL + 1, '0'), level_to_flip_nop_and_up(c_level_to_flip_nop_and_up) {
	checkRepresentation();
}

void EveryStepRewardLadderModel::init() {
	checkRepresentation();
	level = 0;
	coverage.at(0) = '1';
	for (size_t i = 1; i < coverage.size(); ++i) {
		coverage.at(i) = '0';
	}
	checkRepresentation();
}

void EveryStepRewardLadderModel::makeStep(DesignModel::action_type action) {
	checkRepresentation();
	if (coverage.at(level) == '0') {
		coverage.at(level) = '1';
	}

	/* Flipping GO_UP and NOP if level is beyond a threshold named 'level_to_flip_nop_and_up' */
	DesignModel::action_type action_to_perform = action;
	if (level_to_flip_nop_and_up != -1 && level >= level_to_flip_nop_and_up) {
		if (action == NOP) {
			action_to_perform = GO_UP;
		}
		else if (action == GO_UP) {
			action_to_perform = NOP;
		}
	}

	switch (action_to_perform) {
	case NOP:
		break;
	case GO_UP:
		if (level < MAX_LEVEL) {
			++level;
		}
		break;
	case GO_DOWN:
		if (level > 0) {
			--level;
		}
		break;
	default:
		cerr << "No actions other then GO_UP and GO_DOWN should be used in this context, but action \"" << action
				<< "\" was used" << endl;
		exit(1);
	}
	if (coverage.at(level) == '0') {
		coverage.at(level) = '1';
	}
	checkRepresentation();
}

const char** EveryStepRewardLadderModel::getRegistersInNewCStringArray() {
	checkRepresentation();
	string register_string = "";
	register_string.append(bitset<BITS_TO_REPRESENT_LEVEL>(level).to_string());
	char **registers_content = new char*[1];
	registers_content[0] = new char[BITS_TO_REPRESENT_LEVEL + 1];
	strcpy(registers_content[0], register_string.c_str());
	checkRepresentation();
	return (const char**) registers_content;
}
const char** EveryStepRewardLadderModel::getCoverageInNewCStringArray() {
	checkRepresentation();
	char **coverage_content = new char*[1];
	coverage_content[0] = new char[MAX_LEVEL + 2];
	for (size_t i = 0; i < coverage.size(); ++i) {
		strcpy(coverage_content[0] + i, &coverage.at(i));
	}
	coverage_content[0][coverage.size()] = '\0';
	checkRepresentation();
	return (const char**) coverage_content;
}

unsigned int EveryStepRewardLadderModel::getAllowedActionsBitMap() {
	if (level == MAX_LEVEL) {
		return 5; // stands for '101' - yes NOP, no UP, yes DOWN
	}
	if (level == MIN_LEVEL) {
		return 2; // stands for '011' - yes NOP, yes UP, no DOWN
	}
	return 7; // stands for '111' - yes NOP, yes UP, yes DOWN
}

void EveryStepRewardLadderModel::printModel(std::ostream &output_stream) {
	checkRepresentation();
	output_stream << "Level:\t" << level << "\t";
	output_stream << "Coverage:\t";
	for (char cov : coverage) {
		output_stream << cov;
	}
	output_stream << "\n";
}

void EveryStepRewardLadderModel::checkRepresentation() {
	assert(level <= MAX_LEVEL);
	assert(level >= MIN_LEVEL);
	assert(coverage.size() == MAX_LEVEL + 1);
}
