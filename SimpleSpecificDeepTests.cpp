#include "DeepTests.h"

void DeepTests::testCreationOfNDarraysFromExperience() {
	int number_of_registers = 1;
	int number_of_coverages = 1;
	int number_of_actions = 3;
	char **reg;
	char **cov;

	/* state 1*/
	reg = getStringInNewCStringArray(string("0101"));
	cov = getStringInNewCStringArray(string("0000"));
	State state_1(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 2 */
	strcpy(reg[0], string("1110").c_str());
	strcpy(cov[0], string("0001").c_str());
	State state_2(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 3 */
	strcpy(reg[0], string("1100").c_str());
	strcpy(cov[0], string("0001").c_str());
	State state_3(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 4 */
	strcpy(reg[0], string("1001").c_str());
	strcpy(cov[0], string("0011").c_str());
	State state_4(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	delete[] reg[0];
	delete[] cov[0];
	delete[] reg;
	delete[] cov;

	SlidingWindowExperience experience(4);
	experience.addObservation(state_1, 1, 1, state_2);
	experience.addObservation(state_2, 2, 0, state_3);
	experience.addObservation(state_3, 1, 1, state_4);
	experience.addObservation(state_4, 1, 0, state_4);
	experience.printExperience();
	size_t batch_size = 2;
	Shape state_shape(batch_size, state_1.getNumberOfbitsRepresented());
	Shape action_shape(batch_size, number_of_actions);
	Context ctx = Context::gpu();
	NDArray from_state_batch(state_shape, ctx);
	NDArray action_batch(action_shape, ctx);
	NDArray action_with_finality_batch(action_shape, ctx);
	NDArray reward_batch(action_shape, ctx);
	NDArray to_state_batch(state_shape, ctx);
	experience.sampleBatch(from_state_batch, action_batch, reward_batch, to_state_batch, action_with_finality_batch);
	cout << "Current State Batch:\t" << from_state_batch << endl;
	cout << "Action Batch:\t" << action_batch << endl;
	cout << "Action with finality Batch:\t" << action_with_finality_batch << endl;
	cout << "Reward Batch:\t" << reward_batch << endl;
	cout << "Next State Batch:\t" << to_state_batch << endl;
}

char** DeepTests::getStringInNewCStringArray(string str) {
	char **content = new char*[1];
	content[0] = new char[str.length() + 1];
	strcpy(content[0], str.c_str());
	return content;
}

SlidingWindowExperience* DeepTests::createNewTestExperience() {
	int number_of_registers = 1;
	int number_of_coverages = 1;
	char **reg;
	char **cov;

	/* state 1*/
	reg = getStringInNewCStringArray(string("0101"));
	cov = getStringInNewCStringArray(string("0000"));
	State state_1(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 2 */
	strcpy(reg[0], string("1110").c_str());
	strcpy(cov[0], string("0001").c_str());
	State state_2(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 3 */
	strcpy(reg[0], string("1100").c_str());
	strcpy(cov[0], string("0001").c_str());
	State state_3(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 4 */
	strcpy(reg[0], string("1001").c_str());
	strcpy(cov[0], string("0011").c_str());
	State state_4(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	delete[] reg[0];
	delete[] cov[0];
	delete[] reg;
	delete[] cov;

	SlidingWindowExperience *experience = new SlidingWindowExperience(4);
	experience->addObservation(state_1, 1, 1, state_2);
	experience->addObservation(state_2, 2, 0, state_3);
	experience->addObservation(state_3, 1, 1, state_4);
	experience->addObservation(state_4, 1, 0, state_4);

	return experience;
}

void DeepTests::experienceSerializationTest() {
	SlidingWindowExperience *experience = createNewTestExperience();
	cout << "Before:\n" << experience->toString();
	ofstream ofs("test_exp_ser_file");
	boost::archive::text_oarchive oa(ofs);
	oa << *experience;
	ofs.close();
	SlidingWindowExperience experience_deser;
	ifstream ifs("test_exp_ser_file");
	boost::archive::text_iarchive ia(ifs);
	ia >> experience_deser;
//	experience_deser.window.at(2).action = 5;
	cout << "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\nAfter:\n" << experience_deser.toString();
	cout << "\n*****************************\n";
	cout << "Serialization comparison:\t" << experience->isEqual(experience_deser) << endl;
	delete experience;
}
