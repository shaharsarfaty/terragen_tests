/*
 * SimpleDesignModel.cpp
 *
 *  Created on: 24 Sep 2019
 *      Author: Shahar
 */

#include "SimpleDesignModel.h"

using namespace std;

SimpleDesignModel::SimpleDesignModel() : DesignModel(1,1,2) {
	for (int i = 0; i < number_of_coverage_metrics; i++) {
				coverage_metric_names[i] = new char[8];
				string name;
				name.append("success");
//				strcpy_s(coverage_metric_names[i], 8, name.c_str());
				strcpy(coverage_metric_names[i], name.c_str());
			}

	for (int i = 0; i < number_of_actions; i++) {
		action_names[i] = new char[9];
		string name("action_");
		name.append(to_string(i));
//		strcpy_s(action_names[i], 9, name.c_str());
		strcpy(action_names[i], name.c_str());
	}
	checkRepresentation();
}

SimpleDesignModel::~SimpleDesignModel(){
	for (int i = 0; i < number_of_coverage_metrics; i++) {
		delete[] coverage_metric_names[i];
	}
	for (int i = 0; i < number_of_actions; i++) {
		delete[] action_names[i];
	}
}

void SimpleDesignModel::init() {
	level = 0;
	coverage = '0';
}

void SimpleDesignModel::makeStep(DesignModel::action_type action) {
	checkRepresentation();
	switch (action) {
	case GO_UP:
		if(level < MAX_LEVEL) {
			++level;
		}
		break;
	case GO_DOWN:
		if(level > 0) {
			--level;
		}
		break;
	default:
		cerr << "No actions other then GO_UP and GO_DOWN should be used in this context, but action \"" << action << "\" was used" << endl;
		exit(1);
	}
	if(level == MAX_LEVEL) {
		coverage = '1';
	}
}

const char** SimpleDesignModel::getRegistersInNewCStringArray() {
	checkRepresentation();
	std::string register_string = std::bitset<BITS_TO_REPRESENT_LEVEL>(level).to_string();
	char** registers_content = new char* [1];
	registers_content[0] = new char[BITS_TO_REPRESENT_LEVEL + 1];
	strcpy(registers_content[0], register_string.c_str());
	return (const char**)registers_content;
}
const char** SimpleDesignModel::getCoverageInNewCStringArray() {
	checkRepresentation();
	char** coverage_content = new char* [1];
	coverage_content[0] = new char[1 + 1];
	strcpy(coverage_content[0], &coverage);
	return (const char**)coverage_content;
}

unsigned int SimpleDesignModel::getAllowedActionsBitMap() {
	if (level == MAX_LEVEL) {
		return 2; // stands for '10' - no UP yes DOWN
	}
	if (level == MIN_LEVEL) {
		return 1; // stands for '01' - yes UP no DOWN
	}
	return 3; // stands for '11' - yes UP yes DOWN
}

void SimpleDesignModel::printModel(std::ostream& output_stream) {
	checkRepresentation();
	output_stream << "Level:\t" << level << "\t";
	output_stream << "Coverage:\t" << coverage << "\n";
}

void SimpleDesignModel::printMissingCoverageWithNames(ostream& os) {
	string coverage_string(getCoverageInNewCStringArray()[0]);
	if(coverage == '1') {
		os << "All Covered";
	}
	else {
		os << "Didn't cover success";
	}
	os << "\n";

}
void SimpleDesignModel::checkRepresentation(){
	assert(level <= MAX_LEVEL);
	assert(level >= MIN_LEVEL);
}
