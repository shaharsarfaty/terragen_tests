/*
 * SimpleDesignModel.h
 *
 *  Created on: 24 Sep 2019
 *      Author: Shahar
 */

#ifndef SIMPLEDESIGNMODEL_H_
#define SIMPLEDESIGNMODEL_H_

#include "DesignModel.h"
#include <bitset>

class SimpleDesignModel: public DesignModel {
public:
	enum simple_action{
		GO_UP,
		GO_DOWN
	};
private:
	static const int MAX_LEVEL = 15;
	static const int MIN_LEVEL = 0;
	static const int BITS_TO_REPRESENT_LEVEL = 4;
	int level = 0;
	char coverage = '0';
public:
	SimpleDesignModel();
	virtual ~SimpleDesignModel();
	void init();
	void makeStep(action_type action);
	const char** getRegistersInNewCStringArray() override;
	const char** getCoverageInNewCStringArray() override;
	unsigned int getAllowedActionsBitMap() override;
	void printModel(std::ostream& output_stream = std::cout);
	void printMissingCoverageWithNames(std::ostream& os);
private:
	void checkRepresentation();

	/* Testing */
public:
	int getNumberOfLevels() override {return MAX_LEVEL + 1;}
};

#endif /* SIMPLEDESIGNMODEL_H_ */
