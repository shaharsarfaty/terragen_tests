/*
 * DesignModel.h
 *
 *  Created on: 22 Sep 2019
 *      Author: Shahar
 */

#ifndef DESIGNMODEL_H_
#define DESIGNMODEL_H_
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <assert.h>

class DesignModel {
public:
	typedef int action_type;

	int number_of_registers;
	char** registers_names;
	int number_of_coverage_metrics;
	char** coverage_metric_names;
	int number_of_actions;
	char** action_names;

	DesignModel(int c_number_of_registers, int c_number_of_coverage_metrics,
			int c_number_of_actions) :
			number_of_registers(c_number_of_registers),
			registers_names(new char* [number_of_registers]),
			number_of_coverage_metrics(c_number_of_coverage_metrics),
			coverage_metric_names(new char* [number_of_coverage_metrics]),
			number_of_actions(c_number_of_actions),
			action_names(new char* [number_of_actions]) {};
	virtual ~DesignModel() {
		delete[] registers_names;
		delete[] coverage_metric_names;
		delete[] action_names;
	};

	virtual void init() = 0;
	virtual void makeStep(action_type action) = 0;
	virtual const char** getRegistersInNewCStringArray() = 0;
	virtual const char** getCoverageInNewCStringArray() = 0;
	virtual unsigned int getAllowedActionsBitMap() = 0;
	virtual std::size_t getTotalNumberOfBits() {
		return 0;
	}
	virtual void printModel(std::ostream& output_stream = std::cout) = 0;
	virtual int getNumberOfLevels() {return 0;}
//	virtual void printMissingCoverageWithNames(std::ostream& os) = 0;
};

#endif /* DESIGNMODEL_H_ */
