#ifndef TAG_MANAGER_H
#define TAG_MANAGER_H

#include "DesignModel.h"

class TagManagerModel : public DesignModel
{
public:
	enum {
		NOP, GRAB,
		FREE_GRABBED, FREE_NON_GRABBED,
		GRAB_AND_FREE_GRABBED, GRAB_AND_FREE_NON_GRABBED,
		REACHED_TERMINAL_STATE = -1
	};

	typedef enum { REQ_OFF, REQ_ON } request_coverage;
	typedef enum { RETURN_OFF, RETURN_ON} return_coverage;
	typedef enum { FREE_REJECTED, FREE_ACCEPTED} free_coverage;
private:
	const int number_of_tags = 4;
	std::string tags = "0000";
	std::string occupied_tags_coverage[5] = { "000000", "000000", "000000", "000000", "000000" };
	char coverage[5][2][2][2];
public:
	TagManagerModel();
	virtual ~TagManagerModel();
	void init();
	void makeStep(action_type action);
	const char** getRegistersInNewCStringArray();
	const char** getCoverageInNewCStringArray();
	virtual unsigned int getAllowedActionsBitMap() {return 63;} // degenerated - 63 means all actions are allowed
	void printModel(std::ostream& output_stream = std::cout);
	void printMissingCoverageWithNames(std::ostream& os);

private:
	void nop();
	void grab();
	void freeGrabbed();
	void freeNonGrabbed();
	void grabAndFreeGrabbed();
	void grabAndFreeNonGrabbed();
	int getNumberOfOccupiedTags();
	void grabFreeTagIfExisted();

};

#endif // TAG_MANAGER_H
