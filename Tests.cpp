#include "Tests.h"

using namespace std;

// ###########################################
// Testing models
// ###########################################
void Tests::tag_manager_model_test() {
	TagManagerModel tag_manager_model;

	tag_manager_model.init();
	tag_manager_model.printModel();
	tag_manager_model.makeStep(TagManagerModel::FREE_NON_GRABBED);
	tag_manager_model.printModel();
	tag_manager_model.makeStep(TagManagerModel::NOP);
	tag_manager_model.printModel();
	tag_manager_model.makeStep(TagManagerModel::GRAB);
	tag_manager_model.printModel();
	tag_manager_model.makeStep(TagManagerModel::GRAB);
	tag_manager_model.printModel();
}

void Tests::simple_model_test() {
	SimpleDesignModel simple_design_model;

	simple_design_model.init();
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_DOWN);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_DOWN);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_UP);
	simple_design_model.printModel();
	simple_design_model.makeStep(SimpleDesignModel::GO_DOWN);
	simple_design_model.printModel();
}

// ###########################################
// Testing Learning
// ###########################################

void Tests::test_templte(Tests::config& cfg, Tests::DebugFiles& debug_files, DesignModel& model) {
	learner_type learner_t = N_STEP;
	TabularNStepBootstrapLearner::LearningParametersOfNStep* q_n_s_parameters = new TabularNStepBootstrapLearner::LearningParametersOfNStep;
	configToParameters(cfg, *q_n_s_parameters);

	TabularReinforcementLearner* learner = getExampleLearner(learner_t, (TabularReinforcementLearner::LearningParameters*)q_n_s_parameters, model);
	string database_file_name("RL_temp_database_file.txt");

	createDebugFilesHierarchy(cfg, debug_files);

	learner->dumpToFile(database_file_name);
	delete learner;

	learningLoop(learner_t, cfg, (TabularReinforcementLearner::LearningParameters*)q_n_s_parameters, model, database_file_name, debug_files);

	delete q_n_s_parameters;
}


// ###########################################
// Testing Sub-Modules
// ###########################################
void Tests::valuesTableFileUploadDownloadTest(std::ostream& output_stream){
	// building empty values table from scratch
	ValuesTable vt1(1, NULL, 1, NULL, 10, NULL);

	// add entries to table
	string* reg_str = new string("01");
	string* cov_str = new string("10");
	char** reg_content = new char* [1];
	char** cov_content = new char* [1];
	reg_content[0] = new char[3];
	cov_content[0] = new char[3];
	strcpy(reg_content[0], reg_str->c_str());
	strcpy(cov_content[0], cov_str->c_str());
	vt1.getState((const char**)reg_content, (const char**)cov_content, 10);

	// dump to file
	string tmp_dump_file("tmp_dump_file.txt");
	vt1.toFile(tmp_dump_file);

	// build new values table from the file
	ValuesTable vt2(tmp_dump_file);

	// compare both tables
	if(vt1 == vt2 && vt2 == vt1){
		output_stream << "Great! Values table was uploaded and downloaded successfully." << endl;
	}
	else {
		output_stream << "Oops! Values table failed to upload or download." << endl;
	}

	// cleaning up
	delete[] reg_content[0];
	delete[] cov_content[0];
	delete[] reg_content;
	delete[] cov_content;
}


// ###########################################
// Private Helper functions
// ###########################################

TabularReinforcementLearner* Tests::getExampleLearner(learner_type learner_t, TabularReinforcementLearner::LearningParameters* parameters, DesignModel& model){
	TabularReinforcementLearner* learner;
	switch (learner_t) {
	case (TD_ZERO):
		learner = new TabularTDZeroLearner(model.number_of_registers, model.registers_names,
				model.number_of_coverage_metrics, model.coverage_metric_names,
				model.number_of_actions, model.action_names, parameters);
		break;
	case (N_STEP):
		learner = new TabularNStepBootstrapLearner(model.number_of_registers, model.registers_names,
			model.number_of_coverage_metrics, model.coverage_metric_names,
			model.number_of_actions, model.action_names, (TabularNStepBootstrapLearner::LearningParametersOfNStep*)parameters);
		break;
	case(Q_N_STEP):
		learner = new QlearnNStepLearner(model.number_of_registers, model.registers_names,
			model.number_of_coverage_metrics, model.coverage_metric_names,
			model.number_of_actions, model.action_names, (TabularNStepBootstrapLearner::LearningParametersOfNStep*)parameters);
		break;
	default:
		learner = new TabularTDZeroLearner(model.number_of_registers, model.registers_names,
			model.number_of_coverage_metrics, model.coverage_metric_names,
			model.number_of_actions, model.action_names, parameters);
		break;
	}
	
	return learner;
}

TabularReinforcementLearner* Tests::getExampleLearnerFromFile(learner_type learner_t, TabularReinforcementLearner::LearningParameters* parameters, string * filename) {
	TabularReinforcementLearner* learner;
	switch (learner_t) {
	case (TD_ZERO):
		learner = new TabularTDZeroLearner(*filename, parameters);
		break;
	case (N_STEP):
		learner = new TabularNStepBootstrapLearner(*filename, (TabularNStepBootstrapLearner::LearningParametersOfNStep*)parameters);
		break;
	case(Q_N_STEP):
		learner = new QlearnNStepLearner(*filename, (TabularNStepBootstrapLearner::LearningParametersOfNStep*)parameters);
		break;
	default:
		learner = new TabularTDZeroLearner(*filename, parameters);
		break;
	}

	return learner;
}

string Tests::createLogFileName(Tests::config& cfg, const char* prefix, const char* format) {
	string log_file_name(prefix);
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.number_of_steps));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.learning_rate));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.discount_factor));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.initial_epsilon));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.final_part_of_epsilon));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.reward_multiplier));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.negative_incentive_reward));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.number_of_reloadings));
	log_file_name.append("_");
	log_file_name.append(to_string(cfg.max_number_of_steps_before_break));
	log_file_name.append(".");
	log_file_name.append(format);

	return log_file_name;
}

void printRegistersAndCoverageCStrings(int number_of_registers, const char** registers, int number_of_coverages, const char** coverages, std::ostream& os){
	os << "Registers:\t";
	for(int i=0; i<number_of_registers; i++){
		string temp_string_for_printing(registers[i]);
		os << temp_string_for_printing << "\t";
	}
	os << "\nCoverages:\t";
		for(int i=0; i<number_of_coverages; i++){
			string temp_string_for_printing(coverages[i]);
			os << temp_string_for_printing << "\t";
		}
	os << endl;
}

void Tests::learningLoop(learner_type learner_t, Tests::config& cfg, TabularReinforcementLearner::LearningParameters* parameters, DesignModel& model, std::string& database_filename, Tests::DebugFiles& debug_files) {
	TabularReinforcementLearner* learner;
	const char** registers;
	const char** coverage;
	unsigned int allowed_actions;
	int actions_count_per_traverse = 0;
	int action_count_summation_for_average_calc = 0;
	DesignModel::action_type next_action;
	for (int current_traverse = 0; current_traverse < cfg.number_of_reloadings; current_traverse++) {
		parameters->policy_parameter = calculateCurrentEpsilon(cfg.initial_epsilon, cfg.final_part_of_epsilon, current_traverse, cfg.number_of_reloadings);
		learner = getExampleLearnerFromFile(learner_t, parameters, &database_filename);

		model.init();
		registers = model.getRegistersInNewCStringArray();
		coverage = model.getCoverageInNewCStringArray();
		allowed_actions = model.getAllowedActionsBitMap();
		next_action = (DesignModel::action_type)learner->getNextActionForGivenState(registers, coverage, allowed_actions);
		deleteRegistersAndCoverageMetricsCStrings(registers, coverage, model);
		actions_count_per_traverse = 0;
		while (next_action != (DesignModel::action_type)TabularReinforcementLearner::REACHED_TERMINAL_STATE &&
				actions_count_per_traverse < cfg.max_number_of_steps_before_break) {
			model.makeStep(next_action);
			model.printModel(std::cout);
			registers = model.getRegistersInNewCStringArray();
			coverage = model.getCoverageInNewCStringArray();
			allowed_actions = model.getAllowedActionsBitMap();
			next_action = (DesignModel::action_type)learner->getNextActionForGivenState(registers, coverage, allowed_actions);
			deleteRegistersAndCoverageMetricsCStrings(registers, coverage, model);
			actions_count_per_traverse++;
			debugPerAction(current_traverse, actions_count_per_traverse, model, debug_files);
		}
		action_count_summation_for_average_calc += actions_count_per_traverse;
		learner->dumpToFile(database_filename);
		debugPerTraverse(learner, debug_files, current_traverse, actions_count_per_traverse, &action_count_summation_for_average_calc);
		delete learner;
	}
}

void Tests::configToParameters(Tests::config& cfg, TabularNStepBootstrapLearner::LearningParametersOfNStep& params){
	params.learning_rate = cfg.learning_rate;
	params.discount_factor = cfg.discount_factor;
	params.policy = TabularReinforcementLearner::EPSILON_GREEDY;
	params.policy_parameter = cfg.initial_epsilon;
	params.reward_function = TabularReinforcementLearner::MANHATTAN;
	params.reward_parameter = cfg.reward_multiplier;
	params.negative_incentive_reward = cfg.negative_incentive_reward;
	params.number_of_steps = cfg.number_of_steps;
}

void Tests::createDebugFilesHierarchy(Tests::config& cfg, Tests::DebugFiles& debug_files) {
	using namespace boost::gregorian;
	string debug_directory_path(*debug_files.root_path);
	if(debug_directory_path.at(debug_directory_path.size() - 1) != '/'){
		debug_directory_path.append("/");
	}
	debug_directory_path.append("All_Debug/");
	if (!boost::filesystem::exists(debug_directory_path)){
		boost::filesystem::create_directory(debug_directory_path);
	}

	debug_directory_path.append("debug_");
	string time_string(to_iso_extended_string(boost::posix_time::second_clock::local_time()));
	replace(time_string.begin(), time_string.end(), '-', '_');
	replace(time_string.begin(), time_string.end(), ':', '_');
	debug_directory_path.append(time_string);
	/* since time stamp is unique, directory name should be unique */
	assert(!boost::filesystem::exists(debug_directory_path));


	boost::filesystem::create_directory(debug_directory_path);
	debug_directory_path.append("/");
	/* We should only get here in case of initialization */
	assert(debug_files.csv_file == NULL && debug_files.log_file == NULL);

	debug_files.csv_file = new boost::filesystem::ofstream(debug_directory_path + (createLogFileName(cfg, "csv", "csv")));
	debug_files.log_file = new boost::filesystem::ofstream(debug_directory_path + (createLogFileName(cfg, "log", "txt")));
	debug_files.per_action_debug_file = new boost::filesystem::ofstream(debug_directory_path + (createLogFileName(cfg, "per_action_debug", "txt")));
	debug_files.per_traverse_debug_file = new boost::filesystem::ofstream(debug_directory_path + (createLogFileName(cfg, "per_traverse_debug", "txt")));
}

void Tests::deleteRegistersAndCoverageMetricsCStrings(const char** registers, const char** coverage, DesignModel& model){
#ifdef DEBUG_PRINT_FLAG
	printRegistersAndCoverageCStrings(model.number_of_registers, registers, model.number_of_coverage_metrics, coverage, std::cout);
#endif
	for (int i = 0; i < model.number_of_registers; i++) {
		delete[] registers[i];
	}
	for (int i = 0; i < model.number_of_coverage_metrics; i++) {
		delete[] coverage[i];
	}
	delete[] registers;
	delete[] coverage;
}

double Tests::calculateCurrentEpsilon(double initial_epsilon, double final_part_of_epsilon, int current_traverse, int max_number_of_traverses){
	/* Implements Linear decay of epsilon from initial_epsilon to initial_epsilon*final_part_of_epsilon */
	return (initial_epsilon * (1 - ( 1 - final_part_of_epsilon)*(((double)current_traverse) / (double)max_number_of_traverses)));
}

void Tests::debugPerAction(int current_traverse, int actions_count_per_traverse, DesignModel& model, Tests::DebugFiles& debug_files) {
#ifdef DEBUG_PER_ACTION
#ifdef DEBUG_FILE_FLAG
	if (actions_count_per_traverse % 250 == 0) {
		*debug_files.per_action_debug_file << "Iteration: " << current_traverse << ",\tCount: " << actions_count_per_traverse << "\n";
		model.printMissingCoverageWithNames(*debug_files.per_action_debug_file);
	}
#endif
#ifdef DEBUG_PRINT_FLAG
	if (actions_count_per_traverse % 250 == 0) {
		cout << "Iteration: " << current_traverse << ",\tCount: " << actions_count_per_traverse << "\n";
		model.printMissingCoverageWithNames(std::cout);
	}
#endif
#endif
}

void Tests::debugPerTraverse(TabularReinforcementLearner* learner, Tests::DebugFiles& debug_files, int current_traverse, int actions_count_per_traverse, int* action_count_summation_for_average_calc){
assert(learner != NULL);
#ifdef DEBUG_PRINT_FLAG
	learner->printMapInformation(std::cout);
//	learner->printAllMapEntriesCompactly(std::cout);
#endif
#ifdef DEBUG_FILE_FLAG
	learner->printMapInformation(*debug_files.per_traverse_debug_file);
	*debug_files.log_file << "(load: " << current_traverse << ", iterations: " << actions_count_per_traverse << "),\t";
	*debug_files.csv_file << actions_count_per_traverse << ",";
	if (current_traverse % 10 == 9) {
		*debug_files.log_file << "Average of 10: " << (double)*action_count_summation_for_average_calc / 10.0;
		*debug_files.log_file << endl;
		*action_count_summation_for_average_calc = 0;
	}
#endif
}


