#ifndef RL_TESTS_H
#define RL_TESTS_H

#include "TabularTDZeroLearner.h"
#include "TabularNStepBootstrapLearner.h"
#include "QLearnNStepLearner.h"
#include "DesignModel.h"
#include "TagManagerModel.h"
#include "SimpleDesignModel.h"
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>

#define DEBUG_FILE_FLAG
#define DEBUG_PRINT_FLAG
//#define DEBUG_PER_ACTION


class Tests
{
public:

	/* The type of Reinforcement learner - defines the learning strategy */
	typedef enum{TD_ZERO, N_STEP, Q_N_STEP} learner_type;

	/* Configuration structure, to encapsulate both learning hyper-parameters and testing parameters */
	class config {
	public:
		double learning_rate = 0.1;				// the impact of every experience on the change in value [0-1]. 0 is no learning at all.
		double discount_factor = 0.9;			// the importance decay of experience gathered later in the future [0-1]. 0 is no importance for future rewards.
		double initial_epsilon = 0.05;			// the exploration rate [0-1]. 0 is not exploring.
		double reward_multiplier = 1.0;			// how much the actual reward is multiplied in the total reward calculation. [0-inf]
		double negative_incentive_reward = -0.2; // the negative reward earned in every step. [-inf, 0].
		int number_of_steps = 5;				// the number of steps in n-step bootstrapping. [1, inf].

		double final_part_of_epsilon = 0; 		// the final part of epsilon to be reached in last round values reloading and training. [0-1]. 1 means no change.
		int number_of_reloadings = 1000;		// the number of times that values are stored in a file, re-loaded and being learned until convergence (of some kind).
		int max_number_of_steps_before_break = 10000; // the maximal number of steps made by the agent, before calling a round. Typically convergence to full coverage will arrive sooner.
	};

	/* Encapsulation of files used for debug */
	class DebugFiles {
	public:
		std::string* root_path;
		std::ofstream* csv_file;
		std::ofstream* log_file;
		std::ofstream* per_action_debug_file;
		std::ofstream* per_traverse_debug_file;
		~DebugFiles(){
			if(root_path != NULL) delete root_path;
			if(csv_file != NULL) delete csv_file;
			if(log_file != NULL) delete log_file;
			if(per_action_debug_file != NULL) delete per_action_debug_file;
			if(per_traverse_debug_file != NULL) delete per_traverse_debug_file;
		}
	};

public:
	static void tag_manager_model_test();
	static void simple_model_test();
	static void test_templte(Tests::config& cfg, Tests::DebugFiles& debug_files, DesignModel& model);

	static void valuesTableFileUploadDownloadTest(std::ostream& output_stream = std::cout);

private:
	static void createDebugFilesHierarchy(Tests::config& cfg, Tests::DebugFiles& debug_files);
	static TabularReinforcementLearner* getExampleLearner(learner_type learner_t, TabularReinforcementLearner::LearningParameters* parameters, DesignModel& model);
	static TabularReinforcementLearner* getExampleLearnerFromFile(learner_type learner_t, TabularReinforcementLearner::LearningParameters* parameters, std::string* filename = NULL);
	static std::string createLogFileName(Tests::config& cfg, const char* prefix, const char* format);
	static void learningLoop(learner_type learner_t, Tests::config& cfg, TabularReinforcementLearner::LearningParameters* parameters, DesignModel& model, std::string& database_filename, Tests::DebugFiles& debug_files);
	static void configToParameters(Tests::config& cfg, TabularNStepBootstrapLearner::LearningParametersOfNStep& params);
	static void deleteRegistersAndCoverageMetricsCStrings(const char** registers, const char** coverage, DesignModel& model);
	static double calculateCurrentEpsilon(double initial_epsilon, double final_part_of_epsilon, int current_traverse, int max_number_of_traverses);
	static void debugPerAction(int current_traverse, int actions_count_per_traverse, DesignModel& model, Tests::DebugFiles& debug_files);
	static void debugPerTraverse(TabularReinforcementLearner* learner, Tests::DebugFiles& debug_files, int current_traverse, int actions_count_per_traverse, int* action_count_summation_for_average_calc);
};

#endif // RL_TESTS_H
