/*
 * DeepTests.h
 *
 *  Created on: 18 Dec 2019
 *      Author: sarfaty
 */

#ifndef DEEPTESTS_H_
#define DEEPTESTS_H_
#include <string>
#include <string.h>
#include <unistd.h>
#include <unordered_map>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <mxnet-cpp/MxNetCpp.h>
#include "State.h"
#include "SlidingWindowExperience.h"
#include "DeepReinforcementLearner.h"
#include "SimpleDesignModel.h"
#include "EveryStepRewardLadderModel.h"
#include "GridModel.h"

class DeepTests {
public:
	class DebugFiles {
	public:
		std::string *root_path;
		std::unordered_map<std::string, std::ofstream> debug_files_map;
		~DebugFiles() {
			if (root_path != NULL)
				delete root_path;
		}
	};

public:
	/* simple specific tests */
	static void testCreationOfNDarraysFromExperience();
	static char** getStringInNewCStringArray(std::string str);
	static SlidingWindowExperience* createNewTestExperience();
	static void experienceSerializationTest();

	/* ladder test */
	static void firstTestWithExternalSimpleModel();
	static void getStatesListForAccuracyCheck(std::vector<State> &states, DesignModel &model,
			DomainParameters domain_parameters);
	static void getSpecialStatesListForAccuracyCheck64levels(std::vector<State> &states, EveryStepRewardLadderModel &model,
			DomainParameters domain_parameters);
	static void getAllStatesListForAccuracyCheck(std::vector<State> &states, DesignModel &model,
			DomainParameters domain_parameters);
	static void createThreeStatesForGivenCoverageAndPush(std::vector<State> &states, EveryStepRewardLadderModel &model,
			DomainParameters domain_parameters, size_t coverage_up_to_level);
	static void setHumanCraftedActionIfNeeded(int &next_action, size_t episode,
			size_t number_of_first_episodes_to_be_crafted, DeepReinforcementLearner *learner);

	/* Grid test */
	static void gridModelTest();
	static void graphicGridModelTest();
	static void graphicGridLearningTest();
	static void graphicBatchGridLearningTest();
	static void graphicGridTestWithDebug();
	static void gridParametersAssignment(HyperParameters &hyper_parameters, DomainParameters &domain_parameters,
			FilesNames &files_names, GridModel& model);
	static size_t oneFullBatchEpisode(DeepReinforcementLearner **learner, GridModel &model, string &dumping_file_name, Observation& initial_observation);
	static size_t oneFullGraphicEpisode(DeepReinforcementLearner **learner, GridModel &model, string &dumping_file_name, Observation& initial_observation, sf::RenderWindow& window);
	static bool isAllowedControlKey(sf::Event &event);
	static void makeGridStepAccordingToKey(sf::Event &event, GridModel &model);


	/* Generic auxiliaries */
	static void printCurrentProgress(size_t episode_length, size_t loop_count, size_t loop_total);
	static void checkLearningProgress(DeepReinforcementLearner *learner, size_t loop_count,
			std::vector<State> &states_to_evaluate, DebugFiles &debug_files);
	static std::string createActiovationsCsvHeader(DeepReinforcementLearner *learner,
			std::vector<State> &states_to_evaluate);
	static std::string createWeightsCsvHeader(DeepReinforcementLearner *learner);
	static void everyEpisodeDebug(DeepReinforcementLearner *learner, size_t episode, size_t loop_count,
			std::vector<State> &states_to_evaluate, DebugFiles &debug_files);
	static void createDebugFilesHierarchy(HyperParameters &hyper_parameters, DeepTests::DebugFiles &debug_files);
	static void deleteRegsAndCovs(const char **reg, const char **cov, DomainParameters &domain_parameters);
	static float getAverageOverDeque(std::deque<size_t>& deque);
	static float getMedianOverDeque(std::deque<size_t> deque);
};

#endif /* DEEPTESTS_H_ */
