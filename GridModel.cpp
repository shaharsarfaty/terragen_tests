/*
 * GridModel.cpp
 *
 *  Created on: 15 Mar 2020
 *      Author: sarfaty
 */

#include "GridModel.h"
using namespace std;

GridModel::GridModel(int c_x_size, int c_y_size, std::vector<Location> c_checkpoints_by_order, bool c_cover_only_checkpoints,
		Location c_initial_location) :
		DesignModel(2, 1, 5), x_size(c_x_size), y_size(c_y_size), checkpoints_by_order(c_checkpoints_by_order), checkpoints_coverage(
				c_checkpoints_by_order.size(), false), cover_only_checkpoints(c_cover_only_checkpoints), current_location(
				c_initial_location), x_register_content(ceil(log2(c_x_size)), '0'), y_register_content(ceil(log2(c_y_size)),
				'0') {
	number_of_bits_to_represent_register_content = x_register_content.size() + y_register_content.size();
	if (cover_only_checkpoints == true) {
		number_of_bits_to_represent_coverage = checkpoints_by_order.size();
	} else {
		number_of_bits_to_represent_coverage = x_size * y_size;
	}
	coverage = string(number_of_bits_to_represent_coverage, '0');

	grid_map.resize(x_size);
	for (vector<location_state> &col : grid_map) {
		col.resize(y_size);
	}
	m_vertices.setPrimitiveType(sf::Quads);
	m_vertices.resize(x_size * y_size * 4);
	tile_size = sf::Vector2u(40, 40);
	updateGridMap();
	coverThisLocationIfCoverable(current_location);
	checkRep();
}

void GridModel::init() {
	checkRep();
	checkpoints_coverage = vector<bool>(checkpoints_by_order.size(), false);
	current_location.x = 0;
	current_location.y = 0;
	x_register_content = string(ceil(log2(x_size)), '0');
	y_register_content = string(ceil(log2(y_size)), '0');
	coverage = string(number_of_bits_to_represent_coverage, '0');
	assignBinaryValueFromLocationToStrings();
	coverThisLocationIfCoverable(current_location);
	checkRep();
}
void GridModel::makeStep(action_type action) {
	checkRep();
	if (action == NOP) {
		// do nothing
	}
	if (action == GO_UP) {
		if (current_location.y == y_size - 1) {
			// do nothing
		} else {
			current_location.y++;
		}
	}
	if (action == GO_DOWN) {
		if (current_location.y == 0) {
			// do nothing
		} else {
			current_location.y--;
		}
	}
	if (action == GO_RIGHT) {
		if (current_location.x == x_size - 1) {
			// do nothing
		} else {
			current_location.x++;
		}
	}
	if (action == GO_LEFT) {
		if (current_location.x == 0) {
			// do nothing
		} else {
			current_location.x--;
		}
	}
	assignBinaryValueFromLocationToStrings();
	coverThisLocationIfCoverable(current_location);
	checkRep();
}
const char** GridModel::getRegistersInNewCStringArray() {
	checkRep();
	char **registers_content = new char*[2];
	registers_content[0] = new char[x_register_content.size() + 1];
	registers_content[1] = new char[y_register_content.size() + 1];
	strcpy(registers_content[0], x_register_content.c_str());
	strcpy(registers_content[1], y_register_content.c_str());
	checkRep();
	return (const char**) registers_content;
}
const char** GridModel::getCoverageInNewCStringArray() {
	checkRep();
	char **coverage_content = new char*[1];
	coverage_content[0] = new char[coverage.size() + 1];
	strcpy(coverage_content[0], coverage.c_str());
	checkRep();
	return (const char**) coverage_content;
}
unsigned int GridModel::getAllowedActionsBitMap() {
	/* degenerated currently */
	return 0;
}
void GridModel::printModel(std::ostream &output_stream) {
	checkRep();
	output_stream << toString();
	checkRep();
}

string GridModel::toString() {
	checkRep();
	string retStr("");
	retStr.append("x_size:\t" + to_string(x_size) + "\n");
	retStr.append("y_size:\t" + to_string(y_size) + "\n");
	retStr.append("checkpoints_by_order:\t{");
	for (Location checkpoint : checkpoints_by_order) {
		retStr.append(checkpoint.toString());
	}
	retStr.append("}\n");
	retStr.append("checkpoints_coverage:\t{");
	for (bool checkpoint : checkpoints_coverage) {
		retStr.append(checkpoint ? "true " : "false ");
	}
	retStr.append("}\n");
	retStr.append("cover_only_checkpoints:\t" + to_string(cover_only_checkpoints) + "\n");
	retStr.append("current_location:\t" + current_location.toString() + "\n");
	retStr.append("x_register_content:\t" + x_register_content + "\n");
	retStr.append("y_register_content:\t" + y_register_content + "\n");
	retStr.append("coverage:\t" + coverage + "\n");
	retStr.append(
			"number_of_bits_to_represent_register_content:\t" + to_string(number_of_bits_to_represent_register_content)
					+ "\n");
	retStr.append("number_of_bits_to_represent_coverage:\t" + to_string(number_of_bits_to_represent_coverage) + "\n");
	checkRep();
	return retStr;
}

std::string GridModel::mapString() {
	checkRep();
	string retStr("");
	if (cover_only_checkpoints == false) {
		for (int y = y_size - 1; y >= 0; --y) {
			retStr.append("\n");
			for (int x = 0; x < x_size; ++x) {
				if (current_location == Location(x, y)) {
					retStr.append("#");
				} else if (coverage.at(x * x_size + y) == '0') {
					retStr.append("*");
				} else {
					retStr.append("O");
				}
			}
		}
	} else {
		for (int y = y_size - 1; y >= 0; --y) {
			retStr.append("\n");
			for (int x = 0; x < x_size; ++x) {
				if (current_location == Location(x, y)) {
					retStr.append("#");
				} else {
					int checkpoint_index = getCheckpointIndex(Location(x, y));
					if (checkpoint_index == -1) {
						retStr.append("*");
					} else if (checkpoints_coverage.at(checkpoint_index) == true) {
						retStr.append("O");
					} else {
						retStr.append("!");
					}
				}
			}
		}
	}
	checkRep();
	return retStr;
}
void GridModel::checkRep() {
	assert(x_size > 0 && y_size > 0);
	assert(current_location.x >= 0 && current_location.x < x_size);
	assert(current_location.y >= 0 && current_location.y < y_size);
	for (Location &loc : checkpoints_by_order) {
		assert(loc.x >= 0 && loc.x < x_size);
		assert(loc.y >= 0 && loc.y < y_size);
	}
	assert(x_register_content.size() == ceil(log2(x_size)));
	assert(y_register_content.size() == ceil(log2(y_size)));
	for (size_t i = 0; i < x_register_content.size(); ++i) {
		int divided_x = current_location.x;
		for (size_t j = 0; j < i; ++j) {
			divided_x /= 2;
		}
		if (divided_x % 2 == 0) {
			assert(x_register_content.at(i) == '0');
		} else {
			assert(x_register_content.at(i) == '1');
		}
	}
	for (size_t i = 0; i < y_register_content.size(); ++i) {
		int divided_y = current_location.y;
		for (size_t j = 0; j < i; ++j) {
			divided_y /= 2;
		}
		if (divided_y % 2 == 0) {
			assert(y_register_content.at(i) == '0');
		} else {
			assert(y_register_content.at(i) == '1');
		}
	}
	assert(number_of_bits_to_represent_register_content == x_register_content.size() + y_register_content.size());
	assert(number_of_bits_to_represent_coverage == coverage.size());
	if (cover_only_checkpoints == true) {
		assert(number_of_bits_to_represent_coverage == checkpoints_by_order.size());
	} else {
		assert(number_of_bits_to_represent_coverage == size_t(x_size * y_size));
	}
	for (char c : x_register_content) {
		assert(c == '0' || c == '1');
	}
	for (char c : y_register_content) {
		assert(c == '0' || c == '1');
	}
	for (char c : coverage) {
		assert(c == '0' || c == '1');
	}
	assert(checkpoints_by_order.size() == checkpoints_coverage.size());
	for (int i = 0; i < int(checkpoints_coverage.size() - 1); ++i) {
		if (checkpoints_coverage.at(i) == false) {
			assert(checkpoints_coverage.at(i + 1) == false);
		}
	}
	if (cover_only_checkpoints) {
		assert(checkpoints_coverage.size() == coverage.size());
		for (size_t i = 0; i < coverage.size(); ++i) {
			if (checkpoints_coverage.at(i) == false) {
				assert(coverage.at(i) == '0');
			} else {
				assert(coverage.at(i) == '1');
			}
		}
	}
	assert(grid_map.size() == (size_t )x_size);
	for (vector<location_state> &col : grid_map) {
		assert(col.size() == (size_t )y_size);
	}
	int checkpointIndex;
	for (int x = 0; x < x_size; ++x) {
		for (int y = 0; y < y_size; ++y) {
			checkpointIndex = getCheckpointIndex(Location(x, y));
			switch (grid_map.at(x).at(y)) {
			case CURRENT_LOCATION:
				assert(current_location == Location(x, y));
				break;
			case COVERED_CHECKPOINT:
				assert(checkpointIndex != -1 && checkpoints_coverage.at(checkpointIndex) == true);
				break;
			case UNCOVERED_CHECKPOINT:
				assert(checkpointIndex != -1 && checkpoints_coverage.at(checkpointIndex) == false);
				break;
			case COVERED_NORMAL:
				assert(checkpointIndex == -1);
				if (cover_only_checkpoints == false) {
					assert(coverage.at(x * x_size + y) == '1');
				} else {
				}
				break;
			case UNCOVERED_NORMAL:
				assert(checkpointIndex == -1);
				if (cover_only_checkpoints == false) {
					assert(coverage.at(x * x_size + y) == '0');
				}
				break;
			default:
				cerr << "should not get here" << endl;
				exit(-1);
			}

		}
	}

}

void GridModel::coverThisLocationIfCoverable(const Location &location) {
	if (cover_only_checkpoints == true) {
		for (size_t i = 0; i < checkpoints_by_order.size(); ++i) {
			if (location == checkpoints_by_order.at(i)) {
				checkpoints_coverage.at(i) = true;
				coverage.at(i) = '1';
				break;
			} else if (checkpoints_coverage.at(i) == false) {
				break;
			}
		}
	} else if (getCheckpointIndex(location) != -1 && isAllCoveredExceptCheckpoints()) {
		for (size_t i = 0; i < checkpoints_by_order.size(); ++i) {
			if (location == checkpoints_by_order.at(i)) {
				checkpoints_coverage.at(i) = true;
				coverage.at(x_size * location.x + location.y) = '1';
				break;
			} else if (checkpoints_coverage.at(i) == false) {
				break;
			}
		}
	} else {
		coverage.at(x_size * location.x + location.y) = '1';
	}
	updateGridMap();
	checkRep();
}

bool GridModel::isAllCoveredExceptCheckpoints() {
	/* shouldn't get here if we are covering only checkpoints */
	assert(cover_only_checkpoints == false);

	bool allCoveredExceptCheckpoints = true;
	for (int x = 0; x < x_size; ++x) {
		for (int y = 0; y < y_size; ++y) {
			int index_of_checkpoint = getCheckpointIndex(Location(x, y));
			if (index_of_checkpoint != -1) {
				continue;
			} else if (coverage.at(x * x_size + y) == '0') {
				return false;
			}
		}
	}
	return allCoveredExceptCheckpoints;
}

int GridModel::getCheckpointIndex(Location location) {
	int retVal = -1;
	for (size_t i = 0; i < checkpoints_by_order.size(); ++i) {
		if (checkpoints_by_order.at(i) == location) {
			retVal = i;
			break;
		}
	}
	return retVal;
}

void GridModel::updateGridMap() {
	int checkpointIndex;
	for (int x = 0; x < x_size; ++x) {
		for (int y = 0; y < y_size; ++y) {
			checkpointIndex = getCheckpointIndex(Location(x, y));
			if (current_location == Location(x, y)) {
				grid_map.at(x).at(y) = CURRENT_LOCATION;
			} else if (checkpointIndex != -1) {
				if (checkpoints_coverage.at(checkpointIndex) == false) {
					grid_map.at(x).at(y) = UNCOVERED_CHECKPOINT;
				} else {
					grid_map.at(x).at(y) = COVERED_CHECKPOINT;
				}
			} else if (cover_only_checkpoints) {
				grid_map.at(x).at(y) = UNCOVERED_NORMAL;
			} else {
				if (coverage.at(x * x_size + y) == '0') {
					grid_map.at(x).at(y) = UNCOVERED_NORMAL;
				} else {
					grid_map.at(x).at(y) = COVERED_NORMAL;
				}
			}
		}
	}
}

void GridModel::updateVisualStructure() {
	checkRep();
	for (int i = 0; i < x_size; ++i)
		for (int j = 0; j < y_size; ++j) {
			unsigned int vertically_flipped_j = y_size - 1 - j;
			location_state loc_state = grid_map.at(i).at(j);
			sf::Vertex *quad = &m_vertices[(vertically_flipped_j + i * x_size) * 4];

			// define its 4 corners
			quad[0].position = sf::Vector2f(i * tile_size.x, vertically_flipped_j * tile_size.y);
			quad[1].position = sf::Vector2f((i + 1) * tile_size.x, vertically_flipped_j * tile_size.y);
			quad[2].position = sf::Vector2f((i + 1) * tile_size.x, (vertically_flipped_j + 1) * tile_size.y);
			quad[3].position = sf::Vector2f(i * tile_size.x, (vertically_flipped_j + 1) * tile_size.y);

			switch (loc_state) {
			case CURRENT_LOCATION:
				quad[0].color = sf::Color::Blue;
				quad[1].color = sf::Color::Blue;
				quad[2].color = sf::Color::Blue;
				quad[3].color = sf::Color::Blue;
				continue;
			case COVERED_CHECKPOINT:
				quad[0].color = sf::Color::Green;
				quad[1].color = sf::Color::Green;
				quad[2].color = sf::Color::Green;
				quad[3].color = sf::Color::Green;
				continue;
			case UNCOVERED_CHECKPOINT:
				quad[0].color = sf::Color::Red;
				quad[1].color = sf::Color::Red;
				quad[2].color = sf::Color::Red;
				quad[3].color = sf::Color::Red;
				continue;
			case COVERED_NORMAL:
				quad[0].color = sf::Color::Yellow;
				quad[1].color = sf::Color::Yellow;
				quad[2].color = sf::Color::Yellow;
				quad[3].color = sf::Color::Yellow;
				continue;
			case UNCOVERED_NORMAL:
				quad[0].color = sf::Color::White;
				quad[1].color = sf::Color::White;
				quad[2].color = sf::Color::White;
				quad[3].color = sf::Color::White;
				continue;
			default:
				cerr << "bad color key" << endl;
			}
		}
	checkRep();
}

void GridModel::draw(sf::RenderTarget &target, sf::RenderStates states) const {
	states.transform *= getTransform();
	target.draw(m_vertices, states);
}

void GridModel::assignBinaryValueFromLocationToStrings() {
	int divided_x = current_location.x;
	for (size_t bit_index = 0; bit_index < x_register_content.size(); ++bit_index) {
		if (divided_x % 2 == 0) {
			x_register_content.at(bit_index) = '0';
		} else {
			x_register_content.at(bit_index) = '1';
		}
		divided_x /= 2;
	}

	int divided_y = current_location.y;
	for (size_t bit_index = 0; bit_index < y_register_content.size(); ++bit_index) {
		if (divided_y % 2 == 0) {
			y_register_content.at(bit_index) = '0';
		} else {
			y_register_content.at(bit_index) = '1';
		}
		divided_y /= 2;
	}
}
