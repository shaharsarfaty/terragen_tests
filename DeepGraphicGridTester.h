/*
 * DeepGraphicGridTester.h
 *
 *  Created on: 1 Apr 2020
 *      Author: sarfaty
 */

#ifndef DEEPGRAPHICGRIDTESTER_H_
#define DEEPGRAPHICGRIDTESTER_H_

#include "DeepTests.h"
#include "boost/thread.hpp"
using namespace std;
using namespace sf;
using namespace mxnet::cpp;

class DeepGraphicGridTester {

private:
	GridModel model;
	HyperParameters hyper_parameters;
	DomainParameters domain_parameters;
	FilesNames files_names;
	Observation *initial_observation;
	DeepReinforcementLearner *learner;
	RenderWindow window;
	Font font;

	Text instructions_text;
	Text per_episode_text;
	Text action_values_text;
	Text model_string;

	string dumping_file_name = "dumping_file_name";
	int iterations_per_episode;
	deque<size_t> iteration_it_took_for_last_episodes;
	size_t episode_tracking_window_size = 30;
	Event event;
	int learner_suggested_action = -1;
	size_t number_of_episodes_in_a_learning_batch = 1000;
	vector<mx_float> current_q_values;
public:
	DeepGraphicGridTester();
	virtual ~DeepGraphicGridTester();
	void test_1();
private:
	void organizeVisualEntities();
	void assignParametersValues();
	bool isAllowedControlKey();
	void makeGridStepAccordingToKey();
	static void oneFullBatchEpisode(DeepGraphicGridTester* tester);
	int oneFullGraphicEpisode();
	int invisibleBatchLearning();
	int visibleBatchLearning();
	void updatePerEpisode(size_t episode_number);
	void displayOnScreen();
	void updateActionValuesText();
};

#endif /* DEEPGRAPHICGRIDTESTER_H_ */
