/*
 * DeepGraphicGridTester.cpp
 *
 *  Created on: 1 Apr 2020
 *      Author: sarfaty
 */

#include "DeepGraphicGridTester.h"
using namespace std;
using namespace sf;
using namespace mxnet::cpp;

DeepGraphicGridTester::DeepGraphicGridTester() :
		model(4, 4, {GridModel::Location(1,1), GridModel::Location(3,2)}, false), window(sf::VideoMode(1500, 1100), "Graphic Grid Test With Debug") {
	assignParametersValues();
	initial_observation = new Observation((int) domain_parameters.number_of_registers, model.getRegistersInNewCStringArray(),
			(int) domain_parameters.number_of_coverage_metrics, model.getCoverageInNewCStringArray());
	learner = new DeepReinforcementLearner(domain_parameters, hyper_parameters, *initial_observation, files_names);
	font.loadFromFile("/usr/share/fonts/truetype/freefont/FreeSans.ttf");
	organizeVisualEntities();
}

DeepGraphicGridTester::~DeepGraphicGridTester() {
	if (initial_observation != nullptr) {
		delete initial_observation;
	}
	if (learner != nullptr) {
		delete learner;
	}
}

void DeepGraphicGridTester::test_1() {
	bool is_in_control = false;
	while (window.isOpen()) {
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) {
				is_in_control = false;
				if (isAllowedControlKey()) {
					makeGridStepAccordingToKey();
					learner_suggested_action = learner->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
							model.getCoverageInNewCStringArray(), 0);
					is_in_control = true;
				} else if (event.key.code == sf::Keyboard::B) {
					if (invisibleBatchLearning() == -1) {
						cerr << "intrrupted learning thread" << endl;
						window.close();
						return;
					}
				} else if (event.key.code == sf::Keyboard::V) {
					if (visibleBatchLearning() == -1) {
						cerr << "intrrupted learning thread" << endl;
						window.close();
						return;
					}
					// visual batch learn over X episodes
					// show per-step progress, and per episode
				}
			}
		}
		action_values_text.setString("");
		model_string.setString("");
		if (is_in_control) {
			updateActionValuesText();
			model_string.setString(model.toString() + "\n\n" + learner->getCurrentStateString());
		}
		displayOnScreen();
	}
}

// ###############################################################
//		Auxiliaries
// ###############################################################
void DeepGraphicGridTester::organizeVisualEntities() {
	model.setPosition(100, 30);
	instructions_text.setPosition(50, 600);
	per_episode_text.setPosition(50, 450);
	action_values_text.setPosition(600, 80);
	model_string.setPosition(600, 250);

	instructions_text.setFont(font);
	per_episode_text.setFont(font);
	action_values_text.setFont(font);
	model_string.setFont(font);

	instructions_text.setCharacterSize(18);
	per_episode_text.setCharacterSize(18);
	action_values_text.setCharacterSize(18);
	model_string.setCharacterSize(16);

	instructions_text.setFillColor(sf::Color::Cyan);
	per_episode_text.setFillColor(sf::Color::Yellow);
	action_values_text.setFillColor(sf::Color::Red);
	model_string.setFillColor(sf::Color::White);

	instructions_text.setString("For a single step, use the arrows keys\n" "To restart episode, hit R\n"
			"For a visual batch of episodes hit V\n" "For a fast batch of episodes hit B");
}

void DeepGraphicGridTester::assignParametersValues() {
	hyper_parameters.batch_size = 15;
	hyper_parameters.discount_factor = 0.9;
	hyper_parameters.experience_window_size = 80;
	hyper_parameters.weights_initialization_scale = 1;
	hyper_parameters.lr_schedulare.setValue(0.0005);
	hyper_parameters.lr_schedulare.setDecayRate(0.8);
	hyper_parameters.lr_schedulare.setNumberOfStepsBetweenUpdates(500000);
	hyper_parameters.lr_schedulare.setMinimalValue(1e-6);
	hyper_parameters.epsilon_schedulare.setValue(1.0);
	hyper_parameters.epsilon_schedulare.setDecayRate(0.8);
	hyper_parameters.epsilon_schedulare.setNumberOfStepsBetweenUpdates(500000);
	hyper_parameters.epsilon_schedulare.setMinimalValue(0.1);
	hyper_parameters.weight_decay_factor = 1e-4;
	hyper_parameters.hot_ground_incentive_reward = -0.02;
	hyper_parameters.number_of_training_iterations_for_each_observation = 2;
	hyper_parameters.gradient_rescaler = 1.0 / hyper_parameters.batch_size;
	hyper_parameters.gradient_clipping_range = 1.0;
	hyper_parameters.hidden_layers_sizes = { 75, 50 };
	hyper_parameters.number_of_backwords_passes_before_updating_target_network = 5;
	hyper_parameters.use_gpu = true;

	domain_parameters.number_of_registers = 2;
	domain_parameters.number_of_coverage_metrics = 1;
	domain_parameters.total_number_of_state_bits = model.getTotalNumberOfBits();
	domain_parameters.number_of_actions = 5;

	files_names.domain_parameters_file_name = "domain_parameters_dump";
	files_names.experience_file_name = "experience_dump";
	files_names.hyper_parameters_file_name = "hyper_parameters_dump";
	files_names.network_inference_parameters_file_name = "network_inference_parameters_dump";
	files_names.network_training_parameters_file_name = "network_training_parameters_dump";
	files_names.running_parameters_file_name = "running_parameters_dump";
	files_names.initial_observation_file_name = "initial_observation_dump";
}

bool DeepGraphicGridTester::isAllowedControlKey() {
	assert(event.type == sf::Event::KeyPressed);
	if (event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::Down || event.key.code == sf::Keyboard::Right
			|| event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::Numpad0
			|| event.key.code == sf::Keyboard::R) {
		return true;
	} else {
		return false;
	}
}

void DeepGraphicGridTester::makeGridStepAccordingToKey() {
	assert(event.type == sf::Event::KeyPressed);
	assert(isAllowedControlKey());
	switch (event.key.code) {
	case sf::Keyboard::Up:
		model.makeStep(GridModel::grid_action::GO_UP);
		break;
	case sf::Keyboard::Down:
		model.makeStep(GridModel::grid_action::GO_DOWN);
		break;
	case sf::Keyboard::Right:
		model.makeStep(GridModel::grid_action::GO_RIGHT);
		break;
	case sf::Keyboard::Left:
		model.makeStep(GridModel::grid_action::GO_LEFT);
		break;
	case sf::Keyboard::Numpad0:
		model.makeStep(GridModel::grid_action::NOP);
		break;
	case sf::Keyboard::R:
		model.init();
		break;
	default:
		cerr << "[makeGridStepAccordingToKey] should not get here" << endl;
		exit(-1);
	}
}

void DeepGraphicGridTester::oneFullBatchEpisode(DeepGraphicGridTester *tester) {
	try {
		int learner_suggested_action = tester->learner->getNextActionForGivenState(
				tester->model.getRegistersInNewCStringArray(), tester->model.getCoverageInNewCStringArray(), 0);
		tester->iterations_per_episode = 0;
		while (learner_suggested_action != -1) {
			tester->model.makeStep(learner_suggested_action);
			tester->iterations_per_episode++;
			learner_suggested_action = tester->learner->getNextActionForGivenState(
					tester->model.getRegistersInNewCStringArray(), tester->model.getCoverageInNewCStringArray(), 0);
		}
		tester->model.init();
		tester->learner->saveToFiles(tester->dumping_file_name);
		delete tester->learner;
		tester->learner = nullptr;
		tester->learner = new DeepReinforcementLearner(tester->dumping_file_name, *(tester->initial_observation));
	} catch (boost::thread_interrupted&) {
	}
}
int DeepGraphicGridTester::oneFullGraphicEpisode() {
	int next_action = learner->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
			model.getCoverageInNewCStringArray(), 0);
	size_t steps_count = 0;
	sf::Event event;
	while (next_action != -1) {
		displayOnScreen();
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
				delete learner;
				learner = nullptr;
				return -1;
			}
		}
		model.makeStep(next_action);
		++steps_count;
		next_action = learner->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
				model.getCoverageInNewCStringArray(), 0);
	}
	model.init();
	learner->saveToFiles(dumping_file_name);
	delete learner;
	learner = new DeepReinforcementLearner(dumping_file_name, *initial_observation);

	return steps_count;
}

int DeepGraphicGridTester::invisibleBatchLearning() {
	for (size_t episode = 0; episode < number_of_episodes_in_a_learning_batch; ++episode) {
		displayOnScreen();
		boost::thread learning_thread(oneFullBatchEpisode, this);
		while (!learning_thread.joinable()) {
			while (window.pollEvent(event)) {
				if (event.type == sf::Event::Closed) {
					learning_thread.interrupt();
					learning_thread.join();
					if (learner != nullptr) {
						delete learner;
					}
					return -1;
				}
			}
		}
		learning_thread.join();
		updatePerEpisode(episode);
	}
	return 0;
}

int DeepGraphicGridTester::visibleBatchLearning() {
	for (size_t episode = 0; episode < number_of_episodes_in_a_learning_batch; ++episode) {
		displayOnScreen();
		iterations_per_episode = oneFullGraphicEpisode();
		if (iterations_per_episode == -1) {
			return -1;
		}
		updatePerEpisode(episode);
	}
	return 0;
}

void DeepGraphicGridTester::updatePerEpisode(size_t episode_number) {
	if (iteration_it_took_for_last_episodes.size() >= episode_tracking_window_size) {
		iteration_it_took_for_last_episodes.pop_front();
	}
	iteration_it_took_for_last_episodes.push_back(iterations_per_episode);
	per_episode_text.setString(
			"Previous number of steps till completion: " + to_string(iterations_per_episode) + "\nAverage of last "
					+ to_string(episode_tracking_window_size) + " episodes: "
					+ to_string(DeepTests::getAverageOverDeque(iteration_it_took_for_last_episodes)) + "\nMedian of last "
					+ to_string(episode_tracking_window_size) + " episodes: "
					+ to_string(DeepTests::getMedianOverDeque(iteration_it_took_for_last_episodes))
					+ "\nIteration number:\t" + to_string(episode_number) + "/"
					+ to_string(number_of_episodes_in_a_learning_batch) + "\nLearning Rate:\t"
					+ to_string(learner->getLearningRate()) + "\nEpsilon:\t" + to_string(learner->getEpsilon()));
}

void DeepGraphicGridTester::displayOnScreen() {
	window.clear();
	model.updateVisualStructure();
	window.draw(model);
	window.draw(instructions_text);
	window.draw(per_episode_text);
	window.draw(action_values_text);
	window.draw(model_string);
	window.display();
}

void DeepGraphicGridTester::updateActionValuesText() {
	current_q_values = learner->getCurrentStateActionValues();
	action_values_text.setString(
			"NOP:\t" + to_string(current_q_values.at(0)) + "\n" + "UP:\t" + to_string(current_q_values.at(1)) + "\n"
					+ "DOWN:\t" + to_string(current_q_values.at(2)) + "\n" + "RIGHT:\t" + to_string(current_q_values.at(3))
					+ "\n" + "LEFT:\t" + to_string(current_q_values.at(4)) + "\n");
}
