#include "DeepTests.h"

void DeepReinforcementLearner::testNetworksCreation() {
	HyperParameters hyper_parameters;
	hyper_parameters.batch_size = 2;
	hyper_parameters.discount_factor = 0.5;
	hyper_parameters.experience_window_size = 4;

	int number_of_registers = 1;
	int number_of_coverages = 1;
	int number_of_actions = 3;
	char **reg;
	char **cov;

	/* state 1*/
	reg = DeepTests::getStringInNewCStringArray(string("0101"));
	cov = DeepTests::getStringInNewCStringArray(string("0000"));
	State state_1(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 2 */
	strcpy(reg[0], string("1110").c_str());
	strcpy(cov[0], string("0001").c_str());
	State state_2(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 3 */
	strcpy(reg[0], string("1100").c_str());
	strcpy(cov[0], string("0001").c_str());
	State state_3(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	/* state 4 */
	strcpy(reg[0], string("1001").c_str());
	strcpy(cov[0], string("0011").c_str());
	State state_4(number_of_registers, const_cast<const char**>(reg), number_of_coverages, const_cast<const char**>(cov));

	Observation initial_observation((int) number_of_registers, (const char**) reg, (int) number_of_coverages,
			(const char**) cov);

	delete[] reg[0];
	delete[] cov[0];
	delete[] reg;
	delete[] cov;

	DomainParameters domain_parameters;
	domain_parameters.number_of_registers = number_of_registers;
	domain_parameters.number_of_coverage_metrics = number_of_coverages;
	domain_parameters.total_number_of_state_bits = state_1.getNumberOfbitsRepresented();
	domain_parameters.number_of_actions = number_of_actions;

	FilesNames files_names;
	files_names.domain_parameters_file_name = "domain_parameters_dump";
	files_names.experience_file_name = "experience_dump";
	files_names.hyper_parameters_file_name = "hyper_parameters_dump";
	files_names.network_inference_parameters_file_name = "network_inference_parameters_dump";
	files_names.network_training_parameters_file_name = "network_training_parameters_dump";
	files_names.running_parameters_file_name = "running_parameters_dump";

	DeepReinforcementLearner *learner = new DeepReinforcementLearner(domain_parameters, hyper_parameters,
			initial_observation, files_names);
	learner->experience.addObservation(state_1, 1, 1, state_2);
	learner->experience.addObservation(state_2, 2, 0, state_3);
	learner->experience.addObservation(state_3, 1, 1, state_4);
	learner->experience.addObservation(state_4, 1, 0, state_4);

	learner->experience.sampleBatch(learner->train_parameters["state"], learner->train_parameters["actions_taken"],
			learner->infer_parameters["rewards_given"], learner->infer_parameters["state"],
			learner->infer_parameters["actions_taken_discounted"]);

	(learner->infer_parameters["actions_taken_discounted"] * learner->hyper_parameters.discount_factor).CopyTo(
			&learner->infer_parameters["actions_taken_discounted"]);
	NDArray::WaitAll();
	cout << "\nactions:\t\t" << learner->train_parameters["actions_taken"] << endl;
	cout << "\nactions discounted:\t" << learner->infer_parameters["actions_taken_discounted"] << endl;
	learner->train_exec->Forward(true);
	learner->infer_exec->Forward(false);
	NDArray::WaitAll();
	learner->infer_exec->outputs[0].CopyTo(&learner->train_parameters["target"]);
	NDArray::WaitAll();
	cout << "\ntarget:\t\t\t" << learner->train_parameters["target"] << endl;
	learner->train_exec->Backward();

//	learner->train_exec->Forward(false);
//	for (size_t i = 0; i < learner->train_exec->outputs.size(); ++i) {
//		cout << "index: " << i << "\t";
//		cout << learner->train_exec->outputs.at(i) << endl;
//	}

	learner->printTrainingParametersFromExecutor("train");
	learner->printTrainingParametersFromExecutor("infer");
	learner->printAllExecutorOutputs("train");
	learner->printAllExecutorOutputs("infer");

	delete learner;
}

void DeepReinforcementLearner::fullPipestest() {
	HyperParameters hyper_parameters;
	hyper_parameters.batch_size = 2;
	hyper_parameters.discount_factor = 0.5;
	hyper_parameters.experience_window_size = 4;

	DomainParameters domain_parameters;
	domain_parameters.number_of_registers = 1;
	domain_parameters.number_of_coverage_metrics = 1;
	domain_parameters.total_number_of_state_bits = 8;
	domain_parameters.number_of_actions = 4;

	char **reg;
	char **cov;

	/* state 1*/
	reg = DeepTests::getStringInNewCStringArray(string("0000"));
	cov = DeepTests::getStringInNewCStringArray(string("0000"));
	Observation initial_observation((int) domain_parameters.number_of_registers, (const char**) reg,
			(int) domain_parameters.number_of_coverage_metrics, (const char**) cov);

	FilesNames files_names;
	files_names.domain_parameters_file_name = "domain_parameters_dump";
	files_names.experience_file_name = "experience_dump";
	files_names.hyper_parameters_file_name = "hyper_parameters_dump";
	files_names.network_inference_parameters_file_name = "network_inference_parameters_dump";
	files_names.network_training_parameters_file_name = "network_training_parameters_dump";
	files_names.running_parameters_file_name = "running_parameters_dump";

	DeepReinforcementLearner *learner = new DeepReinforcementLearner(domain_parameters, hyper_parameters,
			initial_observation, files_names);

	/* state 2 */
	strcpy(reg[0], string("0001").c_str());
	strcpy(cov[0], string("0001").c_str());
	int next_action = learner->getNextActionForGivenState(const_cast<const char**>(reg), const_cast<const char**>(cov), 0);
	cout << "action 2:\t" << next_action << endl;
	/* state 3 */
	strcpy(reg[0], string("1100").c_str());
	strcpy(cov[0], string("0001").c_str());
	next_action = learner->getNextActionForGivenState(const_cast<const char**>(reg), const_cast<const char**>(cov), 0);
	cout << "action 3:\t" << next_action << endl;

	/* state 4 */
	strcpy(reg[0], string("1001").c_str());
	strcpy(cov[0], string("0001").c_str());
	next_action = learner->getNextActionForGivenState(const_cast<const char**>(reg), const_cast<const char**>(cov), 0);
	cout << "action 4:\t" << next_action << endl;

	/* state 5 */
	strcpy(reg[0], string("1101").c_str());
	strcpy(cov[0], string("0011").c_str());
	next_action = learner->getNextActionForGivenState(const_cast<const char**>(reg), const_cast<const char**>(cov), 0);
	cout << "action 5:\t" << next_action << endl;

	/* state 6 */
	strcpy(reg[0], string("1001").c_str());
	strcpy(cov[0], string("0011").c_str());
	next_action = learner->getNextActionForGivenState(const_cast<const char**>(reg), const_cast<const char**>(cov), 0);
	cout << "action 6:\t" << next_action << endl;

	/* state 7 */
	strcpy(reg[0], string("1111").c_str());
	strcpy(cov[0], string("0111").c_str());
	next_action = learner->getNextActionForGivenState(const_cast<const char**>(reg), const_cast<const char**>(cov), 0);
	cout << "action 7:\t" << next_action << endl;

	delete[] reg[0];
	delete[] cov[0];
	delete[] reg;
	delete[] cov;

	learner->printTrainingParametersFromExecutor("train");
	learner->printTrainingParametersFromExecutor("infer");
	learner->printAllExecutorOutputs("train");
	learner->printAllExecutorOutputs("infer");

	delete learner;
}
