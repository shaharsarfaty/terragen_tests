/*
 * DeepTestsAuxiliaries.cpp
 *
 *  Created on: 31 Mar 2020
 *      Author: sarfaty
 */

#include "DeepTests.h"
using namespace std;
using namespace mxnet::cpp;

void DeepTests::checkLearningProgress(DeepReinforcementLearner *learner, size_t loop_count,
		vector<State> &states_to_evaluate, DebugFiles &debug_files) {
	vector<size_t> checkpoints = { 0, 50, 100, 500, 750, 1000, 2000, 6000, 10000, 30000, 70000, 100000, 200000 };
	string print_header("after ");
	for (size_t check_point : checkpoints) {
		if (loop_count == check_point) {
			print_header.append(to_string(check_point));
			print_header.append(" iterations:\n");
			learner->printTrainingParametersFromExecutor("train", "weights", debug_files.debug_files_map["weights_text"],
					print_header);
			learner->printTrainingParametersFromExecutor("train", "gradients", debug_files.debug_files_map["gradients_text"],
					print_header);
			learner->printAllActionValuesOfGivenStates(states_to_evaluate, debug_files.debug_files_map["activations_text"],
					print_header);
		}
	}
}

string DeepTests::createActiovationsCsvHeader(DeepReinforcementLearner *learner, vector<State> &states_to_evaluate) {
	string csv_header("");
	csv_header.append("Hyper-Parameters:\n");
	csv_header.append(learner->getHyperParameters().toString());
	csv_header.append("Domain-Parameters:\n");
	csv_header.append(learner->getDomainParameters().toString());
	csv_header.append("episode,global_iteration,");
	vector<string> regs;
	vector<string> covs;
	for (State &state : states_to_evaluate) {
		for (const StateElement &reg : state.getRegisters()) {
			regs.push_back(reg.toString());
		}
		for (const StateElement &cov : state.getRegisters()) {
			covs.push_back(cov.toString());
		}
		for (size_t i = 0; i < learner->getDomainParameters().number_of_actions; ++i) {
			csv_header.append(state.toStringSlim() + ",");
		}
	}

	csv_header.append("\nnull,null,");
	for (string &reg : regs) {
		for (size_t i = 0; i < learner->getDomainParameters().number_of_actions; ++i) {
			csv_header.append(reg + ",");
		}
	}
	csv_header.append("\nnull,null,");
	for (string &cov : covs) {
		for (size_t i = 0; i < learner->getDomainParameters().number_of_actions; ++i) {
			csv_header.append(cov + ",");
		}
	}
	csv_header.append("\nnull,null,");
	for (size_t j = 0; j < regs.size(); ++j) {
		for (size_t i = 0; i < learner->getDomainParameters().number_of_actions; ++i) {
			csv_header.append(to_string(i) + ",");
		}
	}
	csv_header.append("\n");
	csv_header.append("#################################################\n");
	return csv_header;
}

string DeepTests::createWeightsCsvHeader(DeepReinforcementLearner *learner) {
	string csv_header("");
	csv_header.append("Hyper-Parameters:\n");
	csv_header.append(learner->getHyperParameters().toString());
	csv_header.append("Domain-Parameters:\n");
	csv_header.append(learner->getDomainParameters().toString());
	csv_header.append("episode,global_iteration,");
	csv_header.append(learner->getWeightsNamesInCsvFormat()); // "\n" included

	return csv_header;
}
void DeepTests::everyEpisodeDebug(DeepReinforcementLearner *learner, size_t episode, size_t loop_count,
		vector<State> &states_to_evaluate, DebugFiles &debug_files) {
	ofstream &activation_csv = debug_files.debug_files_map["activations_csv"];
	ofstream &weights_csv = debug_files.debug_files_map["weights_csv"];
	string header("");
	if (episode == 0) {
		activation_csv << createActiovationsCsvHeader(learner, states_to_evaluate); // "\n" included in header
		weights_csv << createWeightsCsvHeader(learner); // "\n" included in header
	}
	activation_csv << episode << "," << loop_count << ",";
	learner->printAllActionValuesOfGivenStates(states_to_evaluate, activation_csv, header, true);
	weights_csv << episode << "," << loop_count << ",";
	weights_csv << learner->getWeightsInCsvFormat() << endl;
}

void DeepTests::createDebugFilesHierarchy(HyperParameters &hyper_parameters, DeepTests::DebugFiles &debug_files) {
	using namespace boost::gregorian;

	/* creating dubug directory if such does not exist */
	string debug_directory_path(*debug_files.root_path);
	if (debug_directory_path.at(debug_directory_path.size() - 1) != '/') {
		debug_directory_path.append("/");
	}
	debug_directory_path.append("All_Debug/");
	if (!boost::filesystem::exists(debug_directory_path)) {
		boost::filesystem::create_directory(debug_directory_path);
	}

	/* creating simulation specific directory */
	debug_directory_path.append("debug_");
	string time_string(to_iso_extended_string(boost::posix_time::second_clock::local_time()));
	replace(time_string.begin(), time_string.end(), '-', '_');
	replace(time_string.begin(), time_string.end(), ':', '_');
	debug_directory_path.append(time_string);
	/* since time stamp is unique, directory name should be unique */
	assert(!boost::filesystem::exists(debug_directory_path));
	boost::filesystem::create_directory(debug_directory_path);
	debug_directory_path.append("/");

	/*creting sub-directoris for different attricutes */
	string weights_dir_path(debug_directory_path + "weights");
	string grads_dir_path(debug_directory_path + "gradients");
	string activations_dir_path(debug_directory_path + "activations");
	assert(
			!boost::filesystem::exists(weights_dir_path) && !boost::filesystem::exists(weights_dir_path)
					&& !boost::filesystem::exists(activations_dir_path));
	boost::filesystem::create_directory(weights_dir_path);
	weights_dir_path.append("/");
	boost::filesystem::create_directory(grads_dir_path);
	grads_dir_path.append("/");
	boost::filesystem::create_directory(activations_dir_path);
	activations_dir_path.append("/");

	/* We should only get here in case of initialization */
	assert(debug_files.debug_files_map.empty());

	debug_files.debug_files_map["weights_text"] = boost::filesystem::ofstream(weights_dir_path + "/" + "weights.txt");
	debug_files.debug_files_map["weights_csv"] = boost::filesystem::ofstream(weights_dir_path + "/" + "weights.csv");
	debug_files.debug_files_map["gradients_text"] = boost::filesystem::ofstream(grads_dir_path + "/" + "gradients.txt");
	debug_files.debug_files_map["gradients_csv"] = boost::filesystem::ofstream(grads_dir_path + "/" + "gradients.csv");
	debug_files.debug_files_map["activations_text"] = boost::filesystem::ofstream(
			activations_dir_path + "/" + "activations.txt");
	debug_files.debug_files_map["activations_csv"] = boost::filesystem::ofstream(
			activations_dir_path + "/" + "activations.csv");
}

void DeepTests::deleteRegsAndCovs(const char **reg, const char **cov, DomainParameters &domain_parameters) {
	for (size_t j = 0; j < domain_parameters.number_of_registers; ++j) {
		delete[] reg[j];
	}
	delete[] reg;
	for (size_t j = 0; j < domain_parameters.number_of_coverage_metrics; ++j) {
		delete[] cov[j];
	}
	delete[] cov;
}

float DeepTests::getAverageOverDeque(std::deque<size_t>& deque) {
	size_t sum = 0;
	for (size_t element : deque) {
		sum += element;
	}
	return ((float) sum) / ((float) deque.size());
}

float DeepTests::getMedianOverDeque(std::deque<size_t> deque) {
	assert(deque.size() > 0);
	sort(deque.begin(), deque.end());
	return deque.at(deque.size()/2);
}

void DeepTests::printCurrentProgress(size_t episode_length, size_t loop_count, size_t loop_total) {
	cout << "Episode terminated, after\t" << episode_length << "\titerations\t " << loop_count << "/" << loop_total << "\n";
	float progress = (float) loop_count / (float) loop_total;
	float bar_size = 100;
	for (int i = 0; i < bar_size + 2; ++i) {
		cout << "-";
	}
	cout << "\n";
	cout << "|";
	for (float i = 0; i < bar_size; i += 1) {
		if (i / bar_size < progress) {
			cout << "O";
		} else {
			cout << " ";
		}
	}
	cout << "|";
	cout << "\n";
	for (int i = 0; i < bar_size + 2; ++i) {
		cout << "-";
	}
	cout << "\n";
}

