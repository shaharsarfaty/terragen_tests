/*
 * DeepGridTests.cpp
 *
 *  Created on: 31 Mar 2020
 *      Author: sarfaty
 */

#include "DeepTests.h"
using namespace std;
using namespace mxnet::cpp;

void DeepTests::graphicGridLearningTest() {
	sf::RenderWindow window(sf::VideoMode(800, 600), "Grid Learning");

	/* Text */
	sf::Font font;
	font.loadFromFile("/usr/share/fonts/truetype/freefont/FreeSans.ttf");
	sf::Text text;
	text.setFont(font);
	text.setCharacterSize(18);
	text.setFillColor(sf::Color::Yellow);
	text.setPosition(50, 300);
	text.setString("Previous number of steps till completion: ");

	GridModel model(10, 10, { GridModel::Location(2, 2), GridModel::Location(3, 3) }, false);
	model.setPosition(100, 30);

	HyperParameters hyper_parameters;
	hyper_parameters.batch_size = 15;
	hyper_parameters.discount_factor = 0.9;
	hyper_parameters.experience_window_size = 200;
	hyper_parameters.weights_initialization_scale = 1;
	hyper_parameters.lr_schedulare.setValue(0.005);
	hyper_parameters.lr_schedulare.setDecayRate(0.5);
	hyper_parameters.lr_schedulare.setNumberOfStepsBetweenUpdates(100000);
	hyper_parameters.lr_schedulare.setMinimalValue(1e-6);
	hyper_parameters.epsilon_schedulare.setValue(0.9);
	hyper_parameters.epsilon_schedulare.setDecayRate(0.8);
	hyper_parameters.epsilon_schedulare.setNumberOfStepsBetweenUpdates(1000);
	hyper_parameters.epsilon_schedulare.setMinimalValue(0.1);
	hyper_parameters.weight_decay_factor = 1e-4;
	hyper_parameters.hot_ground_incentive_reward = -0.02;
	hyper_parameters.number_of_training_iterations_for_each_observation = 6;
	hyper_parameters.gradient_rescaler = 1.0 / hyper_parameters.batch_size;
	hyper_parameters.gradient_clipping_range = 1.0;
	hyper_parameters.hidden_layers_sizes = { 100, 100, 30 };
	hyper_parameters.number_of_backwords_passes_before_updating_target_network = 5;
	DomainParameters domain_parameters;
	domain_parameters.number_of_registers = 2;
	domain_parameters.number_of_coverage_metrics = 1;
	domain_parameters.total_number_of_state_bits = model.getTotalNumberOfBits();
	domain_parameters.number_of_actions = 5;
	FilesNames files_names;
	files_names.domain_parameters_file_name = "domain_parameters_dump";
	files_names.experience_file_name = "experience_dump";
	files_names.hyper_parameters_file_name = "hyper_parameters_dump";
	files_names.network_inference_parameters_file_name = "network_inference_parameters_dump";
	files_names.network_training_parameters_file_name = "network_training_parameters_dump";
	files_names.running_parameters_file_name = "running_parameters_dump";

	Observation initial_observation((int) domain_parameters.number_of_registers, model.getRegistersInNewCStringArray(),
			(int) domain_parameters.number_of_coverage_metrics, model.getCoverageInNewCStringArray());
	DeepReinforcementLearner *learner = new DeepReinforcementLearner(domain_parameters, hyper_parameters,
			initial_observation, files_names);
	string dumping_file_name = "dumping_file_name";
	int next_action;
	size_t loop_count = 0, iterations = 0, total_iterations = 100000, episode = 0;
	sf::Event event;
	while (window.isOpen()) {
		do {
			while (window.pollEvent(event)) {
				if (event.type == sf::Event::Closed) {
					window.close();
					delete learner;
					return;
				}
			}
			window.clear();
			model.updateVisualStructure();
			window.draw(model);
			window.draw(text);
			window.display();
			next_action = learner->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
					model.getCoverageInNewCStringArray(), 0);
			if (next_action == -1) {
				printCurrentProgress(iterations, loop_count, total_iterations);
				bool event_done = false;
				while (!event_done) {
					while (window.pollEvent(event)) {
						if (event.type == sf::Event::Closed) {
							window.close();
							delete learner;
							return;
						} else if (event.type == sf::Event::KeyPressed) {
							if (event.key.code == sf::Keyboard::Space) {
								event_done = true;
								break;
							}
						}
					}
				}
				text.setString("Previous number of steps till completion: " + to_string(iterations));
				model.init();
				learner->saveToFiles(dumping_file_name);
				delete learner;
				learner = new DeepReinforcementLearner(dumping_file_name, initial_observation);
				++loop_count;
				iterations = 0;
				++episode;
				continue;
			}
			model.makeStep(next_action);
			++loop_count;
			++iterations;
		} while (loop_count < total_iterations);

	}
	delete learner;
}

size_t DeepTests::oneFullBatchEpisode(DeepReinforcementLearner **learner, GridModel &model, string &dumping_file_name,
		Observation &initial_observation) {

	int next_action = (*learner)->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
			model.getCoverageInNewCStringArray(), 0);
	size_t steps_count = 0;
	while (next_action != -1) {
		model.makeStep(next_action);
		++steps_count;
		next_action = (*learner)->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
				model.getCoverageInNewCStringArray(), 0);
	}
	model.init();
	(*learner)->saveToFiles(dumping_file_name);
	delete (*learner);
	(*learner) = new DeepReinforcementLearner(dumping_file_name, initial_observation);

	return steps_count;
}

void DeepTests::graphicGridModelTest() {
	sf::RenderWindow window(sf::VideoMode(800, 600), "Tilemap");
	GridModel model(10, 10, { GridModel::Location(4, 4), GridModel::Location(7, 8) }, false);
	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) {
				switch (event.key.code) {
				case sf::Keyboard::Up:
					model.makeStep(GridModel::grid_action::GO_UP);
					break;
				case sf::Keyboard::Down:
					model.makeStep(GridModel::grid_action::GO_DOWN);
					break;
				case sf::Keyboard::Right:
					model.makeStep(GridModel::grid_action::GO_RIGHT);
					break;
				case sf::Keyboard::Left:
					model.makeStep(GridModel::grid_action::GO_LEFT);
					break;
				case sf::Keyboard::Numpad0:
					model.makeStep(GridModel::grid_action::NOP);
					break;
				default:
					model.makeStep(GridModel::grid_action::NOP);
				}
			}
			break;
		}
		window.clear();
		model.updateVisualStructure();
		window.draw(model);
		window.display();
	}
}

void DeepTests::graphicBatchGridLearningTest() {
	sf::RenderWindow window(sf::VideoMode(800, 600), "Grid Learning");
	/* Text */
	sf::Font font;
	font.loadFromFile("/usr/share/fonts/truetype/freefont/FreeSans.ttf");
	sf::Text iterations_text;
	iterations_text.setFont(font);
	iterations_text.setCharacterSize(18);
	iterations_text.setFillColor(sf::Color::Yellow);
	iterations_text.setPosition(50, 300);
	iterations_text.setString("Previous number of steps till completion: ");

	sf::Text instructions_text;
	instructions_text.setFont(font);
	instructions_text.setCharacterSize(18);
	instructions_text.setFillColor(sf::Color::Cyan);
	instructions_text.setPosition(50, 500);
	instructions_text.setString("For a single episode hit SPACE.\n For a batch of episodes hit b");
	GridModel model(4, 4, { GridModel::Location(0, 0) }, false);
	model.setPosition(100, 30);

	HyperParameters hyper_parameters;
	DomainParameters domain_parameters;
	FilesNames files_names;
	gridParametersAssignment(hyper_parameters, domain_parameters, files_names, model);

	Observation initial_observation((int) domain_parameters.number_of_registers, model.getRegistersInNewCStringArray(),
			(int) domain_parameters.number_of_coverage_metrics, model.getCoverageInNewCStringArray());
	DeepReinforcementLearner *learner = new DeepReinforcementLearner(domain_parameters, hyper_parameters,
			initial_observation, files_names);
	string dumping_file_name = "dumping_file_name";
	size_t iterations_it_took_to_finish_an_episode;
	deque<size_t> iteration_it_took_for_last_episodes;
	iteration_it_took_for_last_episodes.clear();
	size_t episode_tracking_window_size = 10;
	sf::Event event;
	while (window.isOpen()) {
		window.clear();
		model.updateVisualStructure();
		window.draw(model);
		window.draw(iterations_text);
		window.draw(instructions_text);
		window.display();
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
				delete learner;
				return;
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					iterations_it_took_to_finish_an_episode = oneFullGraphicEpisode(&learner, model, dumping_file_name,
							initial_observation, window);
					if (iteration_it_took_for_last_episodes.size() >= episode_tracking_window_size) {
						iteration_it_took_for_last_episodes.pop_front();
					}
					iteration_it_took_for_last_episodes.push_back(iterations_it_took_to_finish_an_episode);
					iterations_text.setString(
							"Previous number of steps till completion: " + to_string(iterations_it_took_to_finish_an_episode)
									+ "\n Average of last 10 episodes: "
									+ to_string(getAverageOverDeque(iteration_it_took_for_last_episodes)));
					window.clear();
					model.updateVisualStructure();
					window.draw(model);
					window.draw(iterations_text);
					window.draw(instructions_text);
					window.display();
				} else if (event.key.code == sf::Keyboard::B) {
					for (int i = 0; i < 1000; ++i) {
						iterations_it_took_to_finish_an_episode = oneFullBatchEpisode(&learner, model, dumping_file_name,
								initial_observation);
						if (iteration_it_took_for_last_episodes.size() >= episode_tracking_window_size) {
							iteration_it_took_for_last_episodes.pop_front();
						}
						iteration_it_took_for_last_episodes.push_back(iterations_it_took_to_finish_an_episode);
						iterations_text.setString(
								"Previous number of steps till completion: "
										+ to_string(iterations_it_took_to_finish_an_episode)
										+ "\nAverage of last 10 episodes: "
										+ to_string(getAverageOverDeque(iteration_it_took_for_last_episodes))
										+ "\nIteration number:\t" + to_string(i) + "/" + to_string(1000)
										+ "\nLearning Rate:\t" + to_string(learner->getLearningRate()) + "\nEpsilon:\t"
										+ to_string(learner->getEpsilon()));
						window.clear();
						model.updateVisualStructure();
						window.draw(model);
						window.draw(iterations_text);
						window.display();
					}
				}
			}
		}
	}
	delete learner;
}

bool DeepTests::isAllowedControlKey(sf::Event &event) {
	assert(event.type == sf::Event::KeyPressed);
	if (event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::Down || event.key.code == sf::Keyboard::Right
			|| event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::Numpad0) {
		return true;
	} else {
		return false;
	}
}

void DeepTests::makeGridStepAccordingToKey(sf::Event &event, GridModel &model) {
	assert(event.type == sf::Event::KeyPressed);
	assert(isAllowedControlKey(event));
	switch (event.key.code) {
	case sf::Keyboard::Up:
		model.makeStep(GridModel::grid_action::GO_UP);
		break;
	case sf::Keyboard::Down:
		model.makeStep(GridModel::grid_action::GO_DOWN);
		break;
	case sf::Keyboard::Right:
		model.makeStep(GridModel::grid_action::GO_RIGHT);
		break;
	case sf::Keyboard::Left:
		model.makeStep(GridModel::grid_action::GO_LEFT);
		break;
	case sf::Keyboard::Numpad0:
		model.makeStep(GridModel::grid_action::NOP);
		break;
	default:
		cerr << "[makeGridStepAccordingToKey] should not get here" << endl;
		exit(-1);
	}
}

void DeepTests::graphicGridTestWithDebug() {
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "Graphic Grid Test With Debug");
	GridModel model(4, 4, { GridModel::Location(4, 4), GridModel::Location(7, 8) }, false);


	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) {
				if (isAllowedControlKey(event)) {
					makeGridStepAccordingToKey(event, model);
					// learn from it
				} else if (event.key.code == sf::Keyboard::B) {
					// batch learn over X episodes, in a separate thread
					// show per episode progress
					// send window there for showing capabilities
					// return if interrupted or finished
				} else if (event.key.code == sf::Keyboard::V) {
					// visual batch learn over X episodes
					// show per-step progress, and per episode
					// send window of course
					// return if interrupted or finished
				}
			}
		}
		// infer Q-values for current state
		// put them to text

		// show model current state
		// show per-episode information
		// show Q-values of given state
		window.clear();
		model.updateVisualStructure();
		window.draw(model);
		// draw text
		window.display();
	}
}

size_t DeepTests::oneFullGraphicEpisode(DeepReinforcementLearner **learner, GridModel &model, string &dumping_file_name,
		Observation &initial_observation, sf::RenderWindow &window) {
	int next_action = (*learner)->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
			model.getCoverageInNewCStringArray(), 0);
	size_t steps_count = 0;
	sf::Event event;
	while (next_action != -1) {
		window.clear();
		model.updateVisualStructure();
		window.draw(model);
		window.display();
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
				delete learner;
				return 0;
			}
		}
		model.makeStep(next_action);
		++steps_count;
		next_action = (*learner)->getNextActionForGivenState(model.getRegistersInNewCStringArray(),
				model.getCoverageInNewCStringArray(), 0);
	}
	model.init();
	(*learner)->saveToFiles(dumping_file_name);
	delete (*learner);
	(*learner) = new DeepReinforcementLearner(dumping_file_name, initial_observation);

	return steps_count;
}

void DeepTests::gridModelTest() {
	GridModel model(10, 10, { GridModel::Location(4, 4), GridModel::Location(7, 8) }, true);
	cout << model.mapString() << endl << endl << model.toString() << endl << endl;
	model.makeStep(GridModel::GO_UP);
	model.makeStep(GridModel::GO_UP);
	model.makeStep(GridModel::GO_RIGHT);
	model.makeStep(GridModel::GO_RIGHT);
	model.makeStep(GridModel::GO_LEFT);
	model.makeStep(GridModel::GO_UP);
	model.makeStep(GridModel::GO_UP);
	model.makeStep(GridModel::GO_RIGHT);
	model.makeStep(GridModel::GO_RIGHT);
	model.makeStep(GridModel::GO_RIGHT);
	model.makeStep(GridModel::GO_RIGHT);
	model.makeStep(GridModel::GO_RIGHT);
	cout << model.mapString() << endl << endl << model.toString() << endl << endl;
}

void DeepTests::gridParametersAssignment(HyperParameters &hyper_parameters, DomainParameters &domain_parameters,
		FilesNames &files_names, GridModel &model) {
	hyper_parameters.batch_size = 15;
	hyper_parameters.discount_factor = 0.9;
	hyper_parameters.experience_window_size = 80;
	hyper_parameters.weights_initialization_scale = 1;
	hyper_parameters.lr_schedulare.setValue(0.005);
	hyper_parameters.lr_schedulare.setDecayRate(0.8);
	hyper_parameters.lr_schedulare.setNumberOfStepsBetweenUpdates(50000);
	hyper_parameters.lr_schedulare.setMinimalValue(1e-6);
	hyper_parameters.epsilon_schedulare.setValue(1.0);
	hyper_parameters.epsilon_schedulare.setDecayRate(0.8);
	hyper_parameters.epsilon_schedulare.setNumberOfStepsBetweenUpdates(50000);
	hyper_parameters.epsilon_schedulare.setMinimalValue(0.1);
	hyper_parameters.weight_decay_factor = 1e-4;
	hyper_parameters.hot_ground_incentive_reward = -0.02;
	hyper_parameters.number_of_training_iterations_for_each_observation = 2;
	hyper_parameters.gradient_rescaler = 1.0 / hyper_parameters.batch_size;
	hyper_parameters.gradient_clipping_range = 1.0;
	hyper_parameters.hidden_layers_sizes = { 20, 20 };
	hyper_parameters.number_of_backwords_passes_before_updating_target_network = 5;

	domain_parameters.number_of_registers = 2;
	domain_parameters.number_of_coverage_metrics = 1;
	domain_parameters.total_number_of_state_bits = model.getTotalNumberOfBits();
	domain_parameters.number_of_actions = 5;

	files_names.domain_parameters_file_name = "domain_parameters_dump";
	files_names.experience_file_name = "experience_dump";
	files_names.hyper_parameters_file_name = "hyper_parameters_dump";
	files_names.network_inference_parameters_file_name = "network_inference_parameters_dump";
	files_names.network_training_parameters_file_name = "network_training_parameters_dump";
	files_names.running_parameters_file_name = "running_parameters_dump";
}

