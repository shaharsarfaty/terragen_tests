/*
 * EveryStepRewardLadderModel.h
 *
 *  Created on: 8 Jan 2020
 *      Author: sarfaty
 */

#ifndef EVERYSTEPREWARDLADDERMODEL_H_
#define EVERYSTEPREWARDLADDERMODEL_H_

#include "DesignModel.h"
#include <math.h>
#include <vector>
#include <bitset>

class EveryStepRewardLadderModel: public DesignModel {
public:
	enum ladder_action {
		NOP, GO_UP, GO_DOWN
	};
public:
	EveryStepRewardLadderModel(int c_level_to_flip_nop_and_up = -1);
	virtual ~EveryStepRewardLadderModel() {
	}
private:
	static const int MAX_LEVEL = 63;
	static const int MIN_LEVEL = 0;
	static const int BITS_TO_REPRESENT_LEVEL = 6;
	int level = 0;
	std::vector<char> coverage;
	int level_to_flip_nop_and_up;
public:
	void init();
	void makeStep(action_type action);
	const char** getRegistersInNewCStringArray() override;
	const char** getCoverageInNewCStringArray() override;
	unsigned int getAllowedActionsBitMap() override;
	void printModel(std::ostream &output_stream = std::cout);
//	void printMissingCoverageWithNames(std::ostream& os);
private:
	void checkRepresentation();

	/* Testing */
public:
	int getNumberOfLevels() override {
		return MAX_LEVEL + 1;
	}
	virtual std::size_t getTotalNumberOfBits() override {
		return BITS_TO_REPRESENT_LEVEL + MAX_LEVEL - MIN_LEVEL + 1;
	}
	int getFlippingLevel() {
		return level_to_flip_nop_and_up;
	}
	int getLevel() {
		return level;
	}
};
#endif /* EVERYSTEPREWARDLADDERMODEL_H_ */
