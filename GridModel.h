/*
 * GridModel.h
 *
 *  Created on: 15 Mar 2020
 *      Author: sarfaty
 */

#ifndef GRIDMODEL_H_
#define GRIDMODEL_H_

#include "DesignModel.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <cmath>
#include <vector>

class GridModel: public DesignModel, public sf::Drawable, public sf::Transformable {
public:
	enum grid_action {
		NOP, GO_UP, GO_DOWN, GO_RIGHT, GO_LEFT
	};
	enum location_state {
		CURRENT_LOCATION, COVERED_CHECKPOINT, UNCOVERED_CHECKPOINT, COVERED_NORMAL, UNCOVERED_NORMAL
	};
	class Location {
	public:
		int x = 0;
		int y = 0;
		Location() {
		}
		Location(int c_x, int c_y) :
				x(c_x), y(c_y) {
		}
		Location(const Location &c_location) :
				x(c_location.x), y(c_location.y) {
		}
		bool operator==(const Location& location) const {
			return x == location.x && y == location.y;
		}
		std::string toString() {
			std::string retStr("");
			retStr.append("(" + std::to_string(x) + "," + std::to_string(y) + ")");
			return retStr;
		}
	};
public:
	GridModel(int c_x_size, int c_y_size, std::vector<Location> c_checkpoints_by_order = {},
			bool c_cover_only_checkpoints = false, Location c_initial_location = Location(0, 0));
	virtual ~GridModel() {
	}
private:
	int x_size = 10;
	int y_size = 10;
	std::vector<Location> checkpoints_by_order;
	std::vector<bool> checkpoints_coverage;
	bool cover_only_checkpoints = false;
	Location current_location;
	std::string x_register_content;
	std::string y_register_content;
	std::string coverage;
	size_t number_of_bits_to_represent_register_content = 0;
	size_t number_of_bits_to_represent_coverage = 0;

	std::vector<std::vector<location_state>> grid_map;
	sf::VertexArray m_vertices;
	sf::Vector2u tile_size;

public:
	void init();
	void makeStep(action_type action);
	const char** getRegistersInNewCStringArray() override;
	const char** getCoverageInNewCStringArray() override;
	unsigned int getAllowedActionsBitMap() override;
	void printModel(std::ostream &output_stream = std::cout);
	std::string toString();
	std::string mapString();
	void updateVisualStructure();

//	void printMissingCoverageWithNames(std::ostream& os);
private:
	void checkRep();
	void coverThisLocationIfCoverable(const Location& location);
	bool isAllCoveredExceptCheckpoints();
	int getCheckpointIndex(Location location);
	void updateGridMap();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void assignBinaryValueFromLocationToStrings();
	/* Testing */
public:
	int getXSize() {
		return x_size;
	}
	int getYSize() {
		return y_size;
	}
	Location getCurrentLocation() {
		return current_location;
	}
	bool isCoveringOnlyCheckpoints() {
		return cover_only_checkpoints;
	}
	std::size_t getTotalNumberOfBits() override {
		return number_of_bits_to_represent_register_content + number_of_bits_to_represent_coverage;
	}
};

#endif /* GRIDMODEL_H_ */
